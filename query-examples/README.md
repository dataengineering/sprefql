# Example queries

In the following you will find 4 simple SPREFQL queries. These queries can also be executed in the demo. 

## Query 1

Suppose that we have the following preference: I prefer one movie to another if their genre are the same and the first one runs longer.

This preference can be expressed using the following preference clause:
```text
PREFER (?title1 ?genre1 ?runtime1) 
TO     (?title2 ?genre2 ?runtime2) 
IF (?genre1 = ?genre2 && ?runtime1 > ?runtime2) 
```
This expresses that a tuple `(?title1 ?genre1 ?runtime1)` is preferred from `(?title2 ?genre2 ?runtime2)` if the condition `(?genre1 = ?genre2 && ?runtime1 > ?runtime2)` holds.

This preference clause functions as a solution modifier, and thus the following query selects the best tuples w.r.t. the previous preference:
```text
SELECT ?title ?genre ?runtime 
WHERE {
  ?film a :film .
  ?film :title ?title .
  ?film :genre ?genre .
  ?film :runtime ?runtime.
}
PREFER (?title1 ?genre1 ?runtime1) 
TO     (?title2 ?genre2 ?runtime2) 
IF (?genre1 = ?genre2 && ?runtime1 > ?runtime2) 
```
Notice that in this query the variables `?title1`, `?title2` correspond to variable `?title`, the variables `?genre1`, `?genre2` correspond to variable `?genre` and `?runtime1`, `?runtime` correspond to variable `?runtime`.

The names in the "PREFER" clause need to be distinct from each other, but they do not need to be distinct from the names in the "SELECT" clause. In this manner, the style shown below is also possible, if the query author prefers it.
```text
SELECT ?title ?genre ?runtime 
WHERE {
  ?film a :film .
  ?film :title ?title .
  ?film :genre ?genre .
  ?film :runtime ?runtime.
}
PREFER (?title ?genre ?runtime) 
TO     (?otherTitle ?otherGenre2 ?otherRuntime) 
IF (?genre = ?otherGenre && ?runtime > ?otherRuntime) 
```

The size of the expected result set of this query is equal to the number of the available genres in the dataset. For each genre, the selected film must be the one with the longest runtime.

## Query 2

As a preference condition you can have an EXISTS constraint, as in the following query which expresses 'I prefer
original movies to their sequels'.
```text
SELECT ?film ?title
WHERE {
 ?film a movie:film .
 ?film :title ?title.
}
PREFER (?film1 ?title1)
TO     (?film2 ?title2)
IF EXISTS { ?film1 :sequel ?film2 } 
```
## Query 3

This query uses the AND combinator which combines two preference conditions in a way that the returned results are the most preferred criteria with equal importance. For example, if i have the preference 'given two action movies, I prefer the longest one and more recent one with equal importance' the corresponding query will be the following:
```text
SELECT ?title ?genre ?runtime 
WHERE {
  ?film a :film .
  ?film :title ?title .
  ?film :genre ?genre .
  ?film :runtime ?runtime.
}
PREFER (?title1 ?runtime1 ?year1) 
TO     (?title2 ?runtime2 ?year2) 
IF (?runtime1 > ?runtime2) AND (?year1 > ?year2) 
```

## Query 4

The other preference combinator is the PRIOR TO combinator. This combines two preference conditions in a way that in order to find the best from two tuples, first we compare them with the first condition, and if they are the same with respect to the first condition, then we continue to the second one. This query selects the best solutions with respect to the preference 'Given two action movies, I prefer the one that runs between 115 and 125 minutes. If they are the
same to me according to this criterion, I prefer the ones that they are after 2005.'
```text
SELECT ?title ?genre ?runtime 
WHERE {
  ?film a :film .
  ?film :title ?title .
  ?film :genre ?genre .
  ?film :runtime ?runtime.
}
PREFER (?title1 ?run1 ?year1) 
TO     (?title2 ?run2 ?year2)
IF (?run1 >= 115 && ?run1 <= 125 && (?run2 < 115 || ?run2 > 125)) 
PRIOR TO (?year1 >= 2005 && ?year2 < 2005) 
```


