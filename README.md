# SPREFQL

A Query Language for expressing user-preferences over RDF Databases

---

## Overview

The SPREFQL language is an extension of the SPARQL language that allows the query author to express preferences, in order to get the most preferred results. Preferences are defined using a `PREFERS` solution modifier. For example, the preference "I prefer one movie to another if their genre are the same and the first one runs longer", can be expressed with the following clause:
```
PREFER (?title1 ?genre1 ?runtime1) 
TO     (?title2 ?genre2 ?runtime2) 
IF (?genre1 = ?genre2 && ?runtime1 > ?runtime2) 
```
The reference implementation of a SPREFQL can be found on the [prefers/](https://bitbucket.org/dataengineering/sprefql/src/5912927e8a8b8269f8804ff719b9b6ad57ffbf1d/prefers/) directory. A simple demo can be found in the [prefers-demo/](https://bitbucket.org/dataengineering/sprefql/src/5912927e8a8b8269f8804ff719b9b6ad57ffbf1d/prefers-demo/) directory. Finally, some example SPREFQL queries can be found in the [query-examples/](https://bitbucket.org/dataengineering/sprefql/src/5912927e8a8b8269f8804ff719b9b6ad57ffbf1d/query-examples/) directory.

## Live Demo

A live demo can be found in this link: [http://143.233.226.33:8087/sprefql/](http://143.233.226.33:8087/sprefql/).

## Publication

* A. Troumpoukis, S. Konstantopoulos and A. Charalambidis. An Extension of SPARQL for Expressing Qualitative Preferences. In *Proceedings of the 16th International Semantic Web Conference 2017, Vienna, Austria, October 21-25, 2017*, pages 711--727, 2017. : [(link)](https://arxiv.org/abs/1707.06406)

## Contact

For suggestions, ideas, feedback etc. e-mail me at: antru [at] iit [dot] demokritos [dot] gr.

