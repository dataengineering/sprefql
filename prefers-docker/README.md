# sprefql docker  

This is the docker container for the preferS engine, which is the reference implementation of the SPREFQL language.

To build, issue:
```
$ docker build -t prefers .
```
To run, issue:
```
$ docker run -p <host_port>:8080 prefers
```
Then, you can access it from the following url (version is currently 1.1)
```
http://localhost:<host_port>/prefers-<version>/repositories/sparql
```
To run prefers with your own configuration file issue
```
$ docker run -p <host_port>:8080 -v </path/to/config>:/etc/prefers prefers
```
PreferS can also be deployed by using docker compose. First edit the docker-compose.yml file and replace /path/to/config with the directory that contains your configuration files. Then issue
```
$ docker-compose up -d
```

