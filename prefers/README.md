# README #

This repository is about the formalism of SPREFQL and PREFERS, the reference implementation of a SPREFQL query processor. SPREFQL is a SPARQL extension for expressing preferences. 

### How to run the experiments ###

Requirements: JDK8 and Maven 3.1 or higher. To built, run the following commands:

``
$ cd prefers/
``

``
$ mvn clean pagkage
``

``
$ mvn dependency:copy-dependencies
``

Firstly deploy a local Virtuoso store that contains the LinkedMDB dataset. Our configuration requires that the endpoint should be in http://localhost:8895/sparql. You could change this on the corresponding configuration files in the src/main/resources directory (and the runExperiment.sh file). The dump file that we used can be found in http://users.iit.demokritos.gr/~gmouchakis/dumps/LMDB.tar.gz. After that, run the following command:

``
$ ./runExperiment.sh 
``

The results are appaended in the /tmp/results.csv file. The queryset can be found in the queryset.txt file.

