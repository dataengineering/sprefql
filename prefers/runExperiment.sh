#!/usr/bin/env bash

./src/main/scripts/runQueries.sh \
    ./src/main/resources/config-remote-nl.ttl \
    ./src/main/resources/config-remote-bnl.ttl \
    ./src/main/resources/config-remote-rw.ttl \
    ./src/main/resources/config-remote-bnl-fb.ttl \
    http://localhost:8895/sparql
