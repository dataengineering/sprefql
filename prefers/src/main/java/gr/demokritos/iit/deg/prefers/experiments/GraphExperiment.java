package gr.demokritos.iit.deg.prefers.experiments;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.config.RepositoryFactory;
import org.eclipse.rdf4j.repository.config.RepositoryRegistry;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;

import gr.demokritos.iit.deg.prefers.commons.Language;
import gr.demokritos.iit.deg.prefers.sail.PrefSailImplConfig;


public class GraphExperiment
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LinkedMDBExperiment.class);

    static FileInputStream dataFile, configFile;
    static {
    	try { dataFile = new FileInputStream( GraphExperiment.class.getClassLoader().getResource("dumps/linkedmdb.subset.ttl").getPath() ); }
    	catch( NullPointerException ex ) { ex.printStackTrace(); dataFile = null; }
    	catch( FileNotFoundException ex ) { ex.printStackTrace(); dataFile = null; }

    	try { configFile = new FileInputStream( GraphExperiment.class.getClassLoader().getResource("config-cg.ttl").getPath() ); }
    	catch( NullPointerException ex ) { configFile = null; }
    	catch( FileNotFoundException ex ) { configFile = null; } // This cannot happen if getResource() non-null

    	//try { configFile = new FileInputStream( GraphExperiment.class.getClassLoader().getResource("config-remote-cg.ttl").getPath() ); }
    	//catch( NullPointerException ex ) { configFile = null; }
    	//catch( FileNotFoundException ex ) { configFile = null; }
    }


    public static void main( String[] args )
    throws Exception
    {
        Model configModel = Rio.parse( configFile, "", RDFFormat.TURTLE);
        SailRepositoryConfig config = new SailRepositoryConfig();
        PrefSailImplConfig prefSailConfig = new PrefSailImplConfig( new MemoryStoreConfig() );
        prefSailConfig.parse( configModel, null );
        config.setSailImplConfig( prefSailConfig );

        Optional<RepositoryFactory> factory = RepositoryRegistry.getInstance().get( config.getType() );
        if( !factory.isPresent() ) {
            throw new IOException("Factory not present!");
        }
        Repository repository = factory.get().getRepository( config ) ;
        repository.initialize();
        RepositoryConnection conn = repository.getConnection();
        
        
        Model data = Rio.parse( dataFile, "", RDFFormat.TURTLE);
        for( Statement stmt : data ) { conn.add( stmt ); }

        /*
        logger.info( "BNL RESULTS" );
        TupleQueryResult result = conn.prepareTupleQuery( Language.PrefSPARQL, Queries.sequelMovieQuery1 ).evaluate();
        while( result.hasNext() ) {
            logger.info( result.next().toString() );
        }
        result.close();
 	*/
        
        logger.info( "CG RESULTS" );

        TupleQuery q = conn.prepareTupleQuery( Language.PrefSPARQL, QueriesOld.sequelMovieQuery1 );
        TupleQueryResult result = q.evaluate();
        
        while( result.hasNext() ) {
            logger.info( result.next().toString() );
        }        
        /*
        result = conn.prepareTupleQuery( , Queries.vanillaSequelMovieQuery1 ).evaluate();
        logger.info( "VANILLA RESULTS" );
        while( result.hasNext() ) {
            logger.info( result.next().toString() );
        }
        result.close();
*/
        conn.close();
        
    }
}
