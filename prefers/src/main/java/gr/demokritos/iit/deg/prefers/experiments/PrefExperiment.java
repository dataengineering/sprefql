package gr.demokritos.iit.deg.prefers.experiments;

import gr.demokritos.iit.deg.prefers.commons.Language;
import gr.demokritos.iit.deg.prefers.sail.PrefSailImplConfig;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.config.RepositoryFactory;
import org.eclipse.rdf4j.repository.config.RepositoryRegistry;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by antonis on 6/4/2017.
 */
public class PrefExperiment {

    private static final Logger logger = LoggerFactory.getLogger(PrefExperiment.class);

    private String configFile;
    private String inputFile;

    private Repository repository;

    private int queryResults;
    private long executionTime;

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public int getLatestQueryResults() {
        return queryResults;
    }

    public long getLatestQueryExecutionTime() {
        return executionTime;
    }

    public void initialize() throws IOException {
        logger.info("Configuring repository");

        Model configModel = Rio.parse(new FileInputStream(new File(configFile)), "", RDFFormat.TURTLE);

        SailRepositoryConfig config = new SailRepositoryConfig();

        MemoryStoreConfig memoryStoreConfig = new MemoryStoreConfig();
        PrefSailImplConfig prefSailConfig = new PrefSailImplConfig(memoryStoreConfig);
        prefSailConfig.parse(configModel, null);
        config.setSailImplConfig(prefSailConfig);

        Optional<RepositoryFactory> factory = RepositoryRegistry.getInstance().get(config.getType());

        if (!factory.isPresent()) {
            throw new IOException("Factory not present!");
        }

        repository = factory.get().getRepository(config);
        repository.initialize();

        RepositoryConnection conn = repository.getConnection();

        logger.info("Importing statements in repository");

        Model model = Rio.parse(new FileInputStream(new File(inputFile)), "", RDFFormat.TURTLE);
        for (Statement statement: model) {
            conn.add(statement);
        }
        conn.close();
    }

    public void runQuery(String queryString) {

        RepositoryConnection conn = repository.getConnection();

        logger.info("Executing query");
        logger.info(queryString);

        long start = System.currentTimeMillis();

        TupleQuery query = conn.prepareTupleQuery(Language.PrefSPARQL, queryString);
        TupleQueryResult result = query.evaluate();

        queryResults=0;
        while (result.hasNext()) {
            logger.info(result.next().toString());
            queryResults++;
        }
        long end = System.currentTimeMillis();
        executionTime = end - start;

        logger.info("Found " + queryResults + " results");
        logger.info("Query Execution Time: " + executionTime + " ms");
    }
}
