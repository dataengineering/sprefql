package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.Preference;
import gr.demokritos.iit.deg.prefers.algebra.RemoteQuery;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.query.algebra.*;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonis on 20/4/2017.
 */
public class RemoteSiteEnforcer {

    private Site site;

    public void setSite(Site site) {
        this.site = site;
    }

    public Site getSite() {
        return site;
    }

    public TupleExpr enforceSite(TupleExpr tupleExpr) {

        assert site.isRemote();

        if ( winnowCount(tupleExpr) == 0 ) {
            return new RemoteQuery(tupleExpr, site);
        }
        else {
            if (tupleExpr instanceof Winnow) {
                Winnow expr = new Winnow(new RemoteQuery(((Winnow) tupleExpr).getArg(), site));
                expr.setBase(((Winnow) tupleExpr).getBase());
                expr.setPrefered(((Winnow) tupleExpr).getPrefered());
                expr.setNonPrefered(((Winnow) tupleExpr).getNonPrefered());
                expr.setPreference(enforceSiteInPreference(((Winnow) tupleExpr).getPreference()));
                return expr;
            }
            else {
                TupleExpr enforced = tupleExpr.clone();
                enforced.visit(new AbstractQueryModelVisitor<RuntimeException>() {
                    @Override
                    protected void meetUnaryTupleOperator(UnaryTupleOperator node) throws RuntimeException {
                        super.meetUnaryTupleOperator(node);
                        if (node instanceof Winnow) {
                            Winnow expr = new Winnow(new RemoteQuery(node.getArg(), site));
                            expr.setBase(((Winnow) node).getBase());
                            expr.setPrefered(((Winnow) node).getPrefered());
                            expr.setNonPrefered(((Winnow) node).getNonPrefered());
                            expr.setPreference(enforceSiteInPreference(((Winnow) node).getPreference()));
                            node.replaceWith(expr);
                        }
                    }
                });
                return enforced;
            }
        }
    }

    private Preference enforceSiteInPreference(ValueExpr in) {
        ValueExpr pref = in.clone();
        pref.visit(new AbstractQueryModelVisitor<RuntimeException>() {
            private int winnow_count = 0;
            @Override
            public void meet(Exists node) {
                TupleExpr subquery = node.getSubQuery();
                node.setSubQuery(new RemoteQuery(new Slice(subquery,0,1), site));
            }
        });
        return (Preference) pref;
    }

    private int winnowCount(TupleExpr expr) {
        List list = new ArrayList<>();

        expr.visit(new AbstractQueryModelVisitor<RuntimeException>() {
            @Override
            protected void meetUnaryTupleOperator(UnaryTupleOperator node) {
                if (node instanceof Winnow) {
                    meet((Winnow)node);
                }
                super.meetUnaryTupleOperator(node);
            }

            private void meet(Winnow node) {
                list.add(true);
            }
        });

        int winnow_count = list.size();
        if (winnow_count > 1) {
            throw new RuntimeException("More than one winnow operators");
        }
        return winnow_count;
    }
}
