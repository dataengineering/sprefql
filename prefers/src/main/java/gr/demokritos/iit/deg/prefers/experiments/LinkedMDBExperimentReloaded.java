package gr.demokritos.iit.deg.prefers.experiments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by antonis on 4/7/2017.
 */
public class LinkedMDBExperimentReloaded extends LinkedMDBExperiment {

    private static final Logger logger = LoggerFactory.getLogger(LinkedMDBExperimentReloaded.class);

    private static String all[] = {
            Queries.Q1,
            Queries.Q2,
            Queries.Q3,
            Queries.Q4,
            Queries.Q5,
            Queries.Q6,
            Queries.Q7,
    };

    private static String inners[] = {
            Queries.B1,
            Queries.B2,
            Queries.B4,
            Queries.B5,
            Queries.B7,
    };

    public static void main(String[] args) throws Exception {

        ClassLoader loader = LinkedMDBExperimentReloaded.class.getClassLoader();
        String nlconfig = loader.getResource("config-remote-nl.ttl").getPath();
        String bnlconfig = loader.getResource("config-remote-bnl.ttl").getPath();
        String rwconfig = loader.getResource("config-remote-rw.ttl").getPath();
        String bnlfbconfig = loader.getResource("config-remote-bnl-fb.ttl").getPath();
        String endpoint = "http://localhost:8895/sparql";

        if (args.length < 5) {
            logger.info("Usage: [nl-config] [bnl-config] [rw-config] [bnl-fb-config] [endpoint]");
        }
        else {
            nlconfig = args[0];
            bnlconfig = args[1];
            rwconfig = args[2];
            bnlfbconfig = args[3];
            endpoint = args[4];
        }

        for (String preference_query : inners) {
            runExperimentSimple("IN", endpoint, "", preference_query);
        }
        for (String preference_query : all) {
            runExperiment("NL", nlconfig, "", preference_query);
        }
        for (String preference_query : all) {
            runExperiment("BNL", bnlconfig, "", preference_query);
        }
        for (String preference_query : all) {
            runExperiment("RW", rwconfig, "", preference_query);
        }
        for (String preference_query : all) {
            runExperiment("BNL-FB", bnlfbconfig, "", preference_query);
        }
    }
}
