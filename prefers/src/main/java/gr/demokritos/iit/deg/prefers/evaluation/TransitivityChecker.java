package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.*;
import org.eclipse.rdf4j.query.algebra.*;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by antonis on 7/7/2017.
 */
public class TransitivityChecker {

    private Preference preference;
    private List<Set<Var>> variablepairs = new ArrayList<>();
    private VarCollector collector = new VarCollector();

    public TransitivityChecker(TupleExpr expr) {
        Winnow winnow = GetWinnowVisitor.getWinnow(expr);
        preference = winnow.getPreference();
        for (int i=0; i<winnow.getBase().size(); i++) {
            Set<Var> set = new HashSet<>();
            set.add(new Var(winnow.getPrefered().get(i)));
            set.add(new Var(winnow.getNonPrefered().get(i)));
            variablepairs.add(set);
        }
    }

    public boolean hasTransitivePreference() {
        if (hasExists(preference)) {
            /* extrinsic preference, always prefer the rewrite method */
            return false;
        }
        else {
            Set<Integer> set = traverse(preference);
            Set<Var> vars = collector.getVars(preference);
            if (set != null) {
                if (set.size()*2 == vars.size()) {
                    return true;
                }
                else return false;
            }
            else {
                return false;
            }
        }
    }

    private Set<Integer> traverse(ValueExpr expr) {
        if (expr instanceof Compare) {
            return traverseBasic(expr);
        }
        if (expr instanceof SimplePref) {
            return traverseUnary((SimplePref) expr);
        }
        if (expr instanceof And || expr instanceof MultidimensionalPref || expr instanceof PrioritizedPref) {
            return traverseBinary((BinaryValueOperator) expr);
        }
        return new HashSet<>();
    }

    private Set<Integer> traverseBasic(ValueExpr expr) {
        Set<Integer> set = new HashSet<>();
        Set<Var> vars = collector.getVars(expr);
        if (variablepairs.contains(vars)) {
            set.add(variablepairs.indexOf(vars));
        }
        return set;
    }

    private Set<Integer> traverseUnary(UnaryValueOperator expr) {
        Set<Integer> set = traverse(expr.getArg());
        return set;
    }

    private Set<Integer> traverseBinary(BinaryValueOperator expr) {
        Set<Integer> setLeft = traverse(expr.getLeftArg());
        Set<Integer> setRight = traverse(expr.getRightArg());
        if (setLeft == null || setRight == null) {
            return null;
        }
        if (setLeft.isEmpty() && setRight.isEmpty()) {
            return traverseBasic(expr);
        }
        Set<Integer> union = new HashSet<>();
        union.addAll(setLeft);
        union.addAll(setRight);
        if (union.size() == setLeft.size() + setRight.size()) {
            return union;
        }
        return null;
    }


    private boolean hasExists(Preference preference) {
        return HasExistsPreferenceVisitor.hasExists(preference);
    }

    static class GetWinnowVisitor extends AbstractQueryModelVisitor<RuntimeException> {

        private Winnow winnow = null;

        @Override
        public void meetUnaryTupleOperator(UnaryTupleOperator node) {
            if (node instanceof Winnow) {
                winnow = (Winnow) node;
            }
            else {
                super.meetUnaryTupleOperator(node);
            }
        }

        static public Winnow getWinnow(TupleExpr expr) {
            GetWinnowVisitor visitor = new GetWinnowVisitor();
            expr.visit(visitor);
            return visitor.winnow;
        }
    }

    static class HasExistsPreferenceVisitor extends AbstractQueryModelVisitor<RuntimeException> {

        private boolean hasExists = false;

        @Override
        public void meet(Exists node) {
            hasExists = true;
        }

        static public boolean hasExists(Preference preference) {
            HasExistsPreferenceVisitor visitor = new HasExistsPreferenceVisitor();
            preference.visit(visitor);
            return visitor.hasExists;
        }
    }

    class VarCollector extends AbstractQueryModelVisitor<RuntimeException> {

        Set<Var> collectedVars;

        @Override
        public void meet(Var var) {
            collectedVars.add(var);
        }

        public Set<Var> getVars(ValueExpr expr) {
            collectedVars = new HashSet<>();
            expr.visit(this);
            return collectedVars;
        }

    }
}
