package gr.demokritos.iit.deg.prefers.rewrite;

import gr.demokritos.iit.deg.prefers.algebra.Preference;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.query.algebra.*;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;

import java.util.*;

/**
 * Created by antonis on 3/4/2017.
 */
public class WinnowRewriter {

    private String suffix = UUID.randomUUID().toString().substring(0,7);

    private TupleExpr origGraph;
    private TupleExpr notExistsGraph;
    private ValueExpr condition;
    private ProjectionElemList projectionList;

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public TupleExpr rewrite(Winnow winnow) {
        Winnow expr = checkLists(winnow);

        if (expr.getArg() instanceof Projection || expr.getArg() instanceof Distinct) {

            TupleExpr querybase = expr.getArg();
            TupleExpr notExistsQuerybase = renamePreferedVars(querybase, expr.getBase(), expr.getPrefered());
            ValueExpr conditionOrig = ConditionRewriter.getRewritedCondition(expr);
            conditionOrig = renameCondition(conditionOrig, expr.getBase(), expr.getNonPrefered());
            if (!containsGroup(querybase)) {
                condition = applyExtensionsToCondition(conditionOrig, querybase, notExistsQuerybase);
                origGraph = ((Projection) querybase).getArg();
                notExistsGraph = ((Projection) notExistsQuerybase).getArg();
            }
            else {
                condition = conditionOrig;
                origGraph = querybase;
                notExistsGraph = notExistsQuerybase;
            }
            TupleExpr result = createProjection(createNotExistsExpression(origGraph, notExistsGraph, condition), expr.getBase());
            projectionList = ((Projection) result).getProjectionElemList();

            return result;
        }
        else {
            throw new RuntimeException("Not implemented");
        }
    }

    public TupleExpr getOrigGraph() {
        return origGraph;
    }

    public TupleExpr getNotExistsGraph() {
        return notExistsGraph;
    }

    public ValueExpr getCondition() {
        return condition;
    }

    public ProjectionElemList getProjectionList() {
        return projectionList;
    }

    private TupleExpr renamePreferedVars(TupleExpr expr, List<String> base, List<String> prefered) {
        TupleExpr newexpr = expr.clone();
        try {
            newexpr.visit(new AbstractQueryModelVisitor<Exception>()  {
                @Override
                public void meet(Var node) throws Exception {
                    super.meet(node);
                    if (base.contains(node.getName())) {
                        int i = base.indexOf(node.getName());
                        node.setName(prefered.get(i));
                    }
                    else {
                        node.setName(rename(node.getName()));
                    }
                }
                @Override
                public void meet(ProjectionElem node) throws Exception {
                    super.meet(node);
                    if (base.contains(node.getTargetName())) {
                        int i = base.indexOf(node.getTargetName());
                        node.setTargetName(prefered.get(i));
                    }
                    else {
                        node.setTargetName(rename(node.getTargetName()));
                    }
                    if (base.contains(node.getSourceName())) {
                        int i = base.indexOf(node.getSourceName());
                        node.setSourceName(prefered.get(i));
                    }
                    else {
                        node.setSourceName(rename(node.getSourceName()));
                    }
                }
                @Override
                public void meet(ExtensionElem node) throws Exception {
                    super.meet(node);
                    if (base.contains(node.getName())) {
                        int i = base.indexOf(node.getName());
                        node.setName(prefered.get(i));
                    }
                    else {
                        node.setName(rename(node.getName()));
                    }
                }
                @Override
                public void meet(GroupElem node) throws Exception {
                    super.meet(node);
                    if (base.contains(node.getName())) {
                        int i = base.indexOf(node.getName());
                        node.setName(prefered.get(i));
                    }
                    else {
                        node.setName(rename(node.getName()));
                    }
                }
                @Override
                public void meet(Group node) throws Exception {
                    super.meet(node);
                    Set<String> newBindingNames = new HashSet<>();
                    for (String bindingName: node.getGroupBindingNames()) {
                        if (base.contains(bindingName)) {
                            int i = base.indexOf(bindingName);
                            newBindingNames.add(prefered.get(i));
                        }
                        else {
                            newBindingNames.add(rename(bindingName));
                        }
                    }
                    node.setGroupBindingNames(newBindingNames);
                }

                private String rename(String s) {

                    return s + "_" + suffix;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newexpr;
    }

    private TupleExpr createNotExistsExpression(TupleExpr prefered, TupleExpr nonPrefered, ValueExpr condition) {
        return new Filter(prefered, new Not(new Exists(new Filter(nonPrefered, condition))));
    }

    private TupleExpr createProjection(TupleExpr inner, List<String> base) {
        List<ProjectionElem> elems = new ArrayList<>();
        for (String s: base) {
            elems.add(new ProjectionElem(s));
        }
        return new Projection(inner, new ProjectionElemList(elems));
    }

    private ValueExpr renameCondition(ValueExpr expr, List<String> base, List<String> nonPrefered) {
        ValueExpr newExpr = expr.clone();
        Utils.renameVars(newExpr, nonPrefered, base);
        return newExpr;
    }

    private ValueExpr applyExtensionsToCondition(ValueExpr expr, TupleExpr origGraph, TupleExpr notExistsGraph) {

        Map<String, ValueExpr> extensionMap = new HashMap<>();

        try {
            origGraph.visit(new AbstractQueryModelVisitor<Exception>() {
                @Override
                public void meet(ExtensionElem node) throws Exception {
                    super.meet(node);
                    extensionMap.put(node.getName(), node.getExpr());
                }
            });
            notExistsGraph.visit(new AbstractQueryModelVisitor<Exception>() {
                @Override
                public void meet(ExtensionElem node) throws Exception {
                    super.meet(node);
                    extensionMap.put(node.getName(), node.getExpr());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        ValueExpr newExpr = expr.clone();

        try {
            newExpr.visit(new AbstractQueryModelVisitor<Exception>() {
                @Override
                public void meet(Var node) throws Exception {
                    super.meet(node);
                    if (extensionMap.containsKey(node.getName())) {
                        ValueExpr valueExpr = extensionMap.get(node.getName());
                        node.replaceWith(valueExpr);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return newExpr;
    }

    private Winnow checkLists(Winnow expr) {
        Set<String> allVars = new HashSet<>();
        allVars.addAll(expr.getBase());
        allVars.addAll(expr.getPrefered());
        allVars.addAll(expr.getNonPrefered());

        if (allVars.size() == expr.getBase().size()*3) {
            return expr;
        }
        else {
            Winnow winnow  = expr.clone();
            for (int i=0; i<winnow.getBase().size(); i++) {
                String oldVar = null;
                String newVar = null;

                if (winnow.getPrefered().get(i).equals(winnow.getBase().get(i))) {
                    oldVar = winnow.getPrefered().get(i);
                    newVar = oldVar + "_" + suffix;
                    winnow.getPrefered().set(i, newVar);
                }

                if (winnow.getNonPrefered().get(i).equals(winnow.getBase().get(i))) {
                    oldVar = winnow.getNonPrefered().get(i);
                    newVar = oldVar + "_" + suffix;
                    winnow.getNonPrefered().set(i, newVar);
                }

                if (oldVar != null && newVar != null) {
                    winnow.setPreference(changeVarName(winnow.getPreference(), oldVar, newVar));
                }
            }
            return winnow;
        }
    }

    private boolean containsGroup(TupleExpr expr) {
        Set dummy = new HashSet<>();
        try {
            expr.visit(new AbstractQueryModelVisitor<Exception>() {
                @Override
                public void meet(Group node) throws Exception {
                    super.meet(node);
                    dummy.add(true);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return !(dummy.isEmpty());
    }

    private Preference changeVarName(Preference preference, String oldVar, String newVar) {
        Preference newPreference = (Preference) preference.clone();
        try {
            newPreference.visit(new AbstractQueryModelVisitor<Exception>() {
                String from = oldVar;
                @Override
                public void meet(Var node) throws Exception {
                    if (node.getName().equals(oldVar)) {
                        node.setName(newVar);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newPreference;
    }
}
