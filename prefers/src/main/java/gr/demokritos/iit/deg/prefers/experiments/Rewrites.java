package gr.demokritos.iit.deg.prefers.experiments;

/**
 * Created by antonis on 5/5/2017.
 */
public final class Rewrites {

    public final static String Q1 = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n" +
            "select ?title ?genre (xsd:float(?runtimeStr) as ?runtime)\n" +
            "where {\n" +
            "  ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?s <http://purl.org/dc/terms/title> ?title.\n" +
            "  ?s <http://data.linkedmdb.org/resource/movie/genre> ?g.\n" +
            "  ?g <http://data.linkedmdb.org/resource/movie/film_genre_name> ?genre.\n" +
            "  ?s <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
            "  filter not exists {\n" +
            "    ?s_afb1a82 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "    ?s_afb1a82 <http://purl.org/dc/terms/title> ?title1.\n" +
            "    ?s_afb1a82 <http://data.linkedmdb.org/resource/movie/genre> ?g_afb1a82.\n" +
            "    ?g_afb1a82 <http://data.linkedmdb.org/resource/movie/film_genre_name> ?genre1.\n" +
            "    ?s_afb1a82 <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_afb1a82.\n" +
            "    filter  ((?genre1 = ?genre) && (xsd:float(?runtimeStr_afb1a82) > xsd:float(?runtimeStr)))\n" +
            "  }\n" +
            "}\n" ;

    public final static String Q2 = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n" +
            "select ?title (xsd:float(?runtimeStr) as ?runtime) (xsd:int(substr(?dateStr, 1, 4)) as ?year)\n" +
            "where {\n" +
            "  ?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?a <http://data.linkedmdb.org/resource/movie/genre> ?g.\n" +
            "  ?g <http://data.linkedmdb.org/resource/movie/film_genre_name> \"Action\".\n" +
            "  ?a <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
            "  ?a <http://purl.org/dc/terms/title> ?title.\n" +
            "  ?a <http://purl.org/dc/terms/date> ?dateStr.\n" +
            "filter not exists {\n" +
            "  ?a_040c1db <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?a_040c1db <http://data.linkedmdb.org/resource/movie/genre> ?g_040c1db.\n" +
            "  ?g_040c1db <http://data.linkedmdb.org/resource/movie/film_genre_name> \"Action\".\n" +
            "  ?a_040c1db <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_040c1db.\n" +
            "  ?a_040c1db <http://purl.org/dc/terms/title> ?title1.\n" +
            "  ?a_040c1db <http://purl.org/dc/terms/date> ?dateStr_040c1db.\n" +
            "  filter  ( ((xsd:float(?runtimeStr_040c1db) > xsd:float(?runtimeStr)) && ( !((xsd:int(substr(?dateStr, 1, 4)) > xsd:int(substr(?dateStr_040c1db, 1, 4)))))) \n" +
            "      ||  ((xsd:int(substr(?dateStr_040c1db, 1, 4)) > xsd:int(substr(?dateStr, 1, 4))) && ( !((xsd:float(?runtimeStr) > xsd:float(?runtimeStr_040c1db))))))\n" +
            "}\n" +
            "}\n" ;

    public final static String Q3 = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n" +
            "select ?title (xsd:float(?runtimeStr) as ?runtime) (xsd:int(substr(?dateStr, 1, 4)) as ?year)\n" +
            "where {\n" +
            "  ?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?a <http://data.linkedmdb.org/resource/movie/genre> ?g.\n" +
            "  ?g <http://data.linkedmdb.org/resource/movie/film_genre_name> \"Action\".\n" +
            "  ?a <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
            "  ?a <http://purl.org/dc/terms/title> ?title.\n" +
            "  ?a <http://purl.org/dc/terms/date> ?dateStr.\n" +
            "  filter not exists {\n" +
            "    ?a_0373cbe <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "    ?a_0373cbe <http://data.linkedmdb.org/resource/movie/genre> ?g_0373cbe.\n" +
            "    ?g_0373cbe <http://data.linkedmdb.org/resource/movie/film_genre_name> \"Action\".\n" +
            "    ?a_0373cbe <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_0373cbe.\n" +
            "    ?a_0373cbe <http://purl.org/dc/terms/title> ?title1.\n" +
            "    ?a_0373cbe <http://purl.org/dc/terms/date> ?dateStr_0373cbe.\n" +
            "    filter  ( ((xsd:float(?runtimeStr_0373cbe)>= 115) &&  ((xsd:float(?runtimeStr_0373cbe)<= 125) &&  ((xsd:float(?runtimeStr) < 115) || (xsd:float(?runtimeStr) > 125)))) \n" +
            "\t||  ( (( !( ((xsd:float(?runtimeStr_0373cbe)>= 115) &&  ((xsd:float(?runtimeStr_0373cbe)<= 125) &&  ((xsd:float(?runtimeStr) < 115) \n" +
            "\t|| (xsd:float(?runtimeStr) > 125)))))) && ( !( ((xsd:float(?runtimeStr) >= 115) &&  ((xsd:float(?runtimeStr) <= 125) &&  ((xsd:float(?runtimeStr_0373cbe)< 115) \n" +
            "\t|| (xsd:float(?runtimeStr_0373cbe)> 125))))))) &&  ((xsd:int(substr(?dateStr_0373cbe, 1, 4)) >= 2005) && (xsd:int(substr(?dateStr, 1, 4))  < 2005))))\n" +
            "  }\n" +
            "}\n" ;

    public final static String Q4 = "select ?film ?title\n" +
            "where {\n" +
            "  ?film <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?film <http://purl.org/dc/terms/title> ?title.\n" +
            "  filter  regex(?title, \"Mad Max\").\n" +
            "  filter not exists {\n" +
            "    ?film1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "    ?film1 <http://purl.org/dc/terms/title> ?title1.\n" +
            "    filter  regex(?title1, \"Mad Max\").\n" +
            "    filter  exists {  ?film1 <http://data.linkedmdb.org/resource/movie/sequel> ?film. }\n" +
            "  }\n" +
            "}\n" ;

    public final static String Q5 = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n" +
            "select ?title (xsd:float(?runtimeStr) as ?runtime) (xsd:int(substr(?dateStr, 1, 4)) as ?year)\n" +
            "where {\n" +
            "  ?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?a <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
            "  ?a <http://purl.org/dc/terms/title> ?title.\n" +
            "  ?a <http://purl.org/dc/terms/date> ?dateStr.\n" +
            "filter not exists {\n" +
            "  ?a_040c1db <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?a_040c1db <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_040c1db.\n" +
            "  ?a_040c1db <http://purl.org/dc/terms/title> ?title1.\n" +
            "  ?a_040c1db <http://purl.org/dc/terms/date> ?dateStr_040c1db.\n" +
            "  filter  ( ((xsd:float(?runtimeStr_040c1db) > xsd:float(?runtimeStr)) && ( !((xsd:int(substr(?dateStr, 1, 4)) > xsd:int(substr(?dateStr_040c1db, 1, 4)))))) \n" +
            "      ||  ((xsd:int(substr(?dateStr_040c1db, 1, 4)) > xsd:int(substr(?dateStr, 1, 4))) && ( !((xsd:float(?runtimeStr) > xsd:float(?runtimeStr_040c1db))))))\n" +
            "}\n" +
            "}" ;

    public final static String Q6 = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n" +
            "select ?title (xsd:float(?runtimeStr) as ?runtime) (xsd:int(substr(?dateStr, 1, 4)) as ?year)\n" +
            "where {\n" +
            "  ?a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?a <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
            "  ?a <http://purl.org/dc/terms/title> ?title.\n" +
            "  ?a <http://purl.org/dc/terms/date> ?dateStr.\n" +
            "  filter not exists {\n" +
            "    ?a_0373cbe <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "    ?a_0373cbe <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_0373cbe.\n" +
            "    ?a_0373cbe <http://purl.org/dc/terms/title> ?title1.\n" +
            "    ?a_0373cbe <http://purl.org/dc/terms/date> ?dateStr_0373cbe.\n" +
            "    filter  ( ((xsd:float(?runtimeStr_0373cbe)>= 115) &&  ((xsd:float(?runtimeStr_0373cbe)<= 125) &&  ((xsd:float(?runtimeStr) < 115) || (xsd:float(?runtimeStr) > 125)))) \n" +
            "\t||  ( (( !( ((xsd:float(?runtimeStr_0373cbe)>= 115) &&  ((xsd:float(?runtimeStr_0373cbe)<= 125) &&  ((xsd:float(?runtimeStr) < 115) \n" +
            "\t|| (xsd:float(?runtimeStr) > 125)))))) && ( !( ((xsd:float(?runtimeStr) >= 115) &&  ((xsd:float(?runtimeStr) <= 125) &&  ((xsd:float(?runtimeStr_0373cbe)< 115) \n" +
            "\t|| (xsd:float(?runtimeStr_0373cbe)> 125))))))) &&  ((xsd:int(substr(?dateStr_0373cbe, 1, 4)) >= 2005) && (xsd:int(substr(?dateStr, 1, 4))  < 2005))))\n" +
            "  }\n" +
            "}\n\n" ;;

    public final static String Q7 =  "select ?film ?title\n" +
            "where {\n" +
            "  ?film <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "  ?film <http://purl.org/dc/terms/title> ?title.\n" +
            "  ?film <http://data.linkedmdb.org/resource/movie/performance> ?performance.\n" +
            "  ?performance <http://data.linkedmdb.org/resource/movie/performance_character> ?character.\n" +
            "  FILTER (?character = \"James Bond\").\n" +
            "  filter not exists {\n" +
            "    ?film1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
            "    ?film1 <http://purl.org/dc/terms/title> ?title1.\n" +
            "    ?film1 <http://data.linkedmdb.org/resource/movie/performance> ?performance_1.\n" +
            "    ?performance_1 <http://data.linkedmdb.org/resource/movie/performance_character> ?character_1.\n" +
            "    FILTER (?character_1 = \"James Bond\").\n" +
            "    filter  exists {  ?film1 <http://data.linkedmdb.org/resource/movie/sequel> ?film. }\n" +
            "  }\n" +
            "}\n" ;
}
