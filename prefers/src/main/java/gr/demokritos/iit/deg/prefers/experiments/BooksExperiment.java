package gr.demokritos.iit.deg.prefers.experiments;

import gr.demokritos.iit.deg.prefers.rewrite.PrefSparqlRewriter;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by antonis on 6/4/2017.
 */
public class BooksExperiment {

    private static String NLconfig = "config-nl.ttl";
    private static String BNLconfig = "config-bnl.ttl";

    private static String[] files = {
            "dumps/linkedmdb.subset.ttl",
            //"dumps/books.100.ttl", "dumps/books.1000.ttl", "dumps/books.2000.ttl",
            //"dumps/books.4000.ttl", "dumps/books.8000.ttl", "dumps/books.10000.ttl"
        };

    private static String preference_query = QueriesOld.recentSequelMovieQuery;

    public static void main(String[] args) throws Exception {
        clearTmp();
        for (String file: files) {
            runPrefExperiment("NL",  getPath(NLconfig),  getPath(file), preference_query);
            //runPrefExperiment("BNL", getPath(BNLconfig), getPath(file), preference_query);
            //runSimpExperiment("RW",  getPath(file), rewrite(preference_query));
        }
    }

    protected static void clearTmp() throws IOException {
        Runtime.getRuntime().exec(new String[] { "sh", "-c", "rm /tmp/BNLtmpfile-*.json" });
    }

    protected static void runPrefExperiment(String id, String config, String fileName, String query) throws IOException {
        PrefExperiment experiment = new PrefExperiment();
        experiment.setConfigFile(config);
        experiment.setInputFile(fileName);
        experiment.initialize();
        experiment.runQuery(query);
        writeResults("/tmp/results.csv", id, experiment.getLatestQueryExecutionTime(), experiment.getLatestQueryResults());
    }

    protected static void runSimpExperiment(String id, String fileName, String query) throws IOException {
        System.out.println(query);
        SimpleExperiment experiment = new SimpleExperiment();
        experiment.setBaseSail(new MemoryStore());
        experiment.setInputFile(fileName);
        experiment.initialize();
        experiment.runQuery(query);
        writeResults("/tmp/results.csv", id, experiment.getLatestQueryExecutionTime(), experiment.getLatestQueryResults());
    }

    protected static void runVirtuosoExperiment(String id, String endpointUrl, String query) throws IOException {
        HttpExperiment experiment = new HttpExperiment();
        experiment.runQuery(endpointUrl, query);
        writeResults("/tmp/results.csv", id, experiment.getLatestQueryExecutionTime(), experiment.getLatestQueryResults());
    }

    protected static String getPath(String fileName) {
        return BooksExperiment.class.getClassLoader().getResource(fileName).getPath();
    }

    protected static String rewrite(String query) throws Exception {
        return new PrefSparqlRewriter().rewrite(query);
    }

    protected static void writeResults(String path, String id, long exectime, long results) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File("/tmp/results.csv"), true));
        bw.write(id + ";" + exectime + ";" + results + "\n");
        bw.close();
    }
}
