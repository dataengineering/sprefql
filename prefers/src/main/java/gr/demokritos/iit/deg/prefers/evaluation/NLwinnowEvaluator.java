package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.common.iteration.LookAheadIteration;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.EvaluationStrategy;
import org.eclipse.rdf4j.query.algebra.evaluation.QueryBindingSet;

/**
 * Created by antonis on 29/3/2017.
 */
public class NLwinnowEvaluator implements WinnowEvaluator {

    private PreferenceRelationEvaluator preferenceRelationEvaluator;
    private EvaluationStrategy evaluationStrategy;
    private String positionVarName = "_POSITION_";

    public void setPreferenceRelationEvaluator(PreferenceRelationEvaluator preferenceRelationEvaluator) {
        this.preferenceRelationEvaluator = preferenceRelationEvaluator;
    }

    public void setEvaluationStrategy(EvaluationStrategy evaluationStrategy) {
        this.evaluationStrategy = evaluationStrategy;
    }

    public CloseableIteration<BindingSet,QueryEvaluationException> evaluate(Winnow expr, BindingSet bindings) {

        BindingSetMapper mapper = new BindingSetMapper(expr);
        preferenceRelationEvaluator.setMapper(mapper);

        CloseableIteration<BindingSet,QueryEvaluationException> outerIter = iterWrap(evaluationStrategy.evaluate(expr.getArg(), bindings));

        return iterUnWrap(new LookAheadIteration<BindingSet, QueryEvaluationException>() {

            @Override
            protected BindingSet getNextElement() throws QueryEvaluationException {

                while (outerIter.hasNext()) {
                    BindingSet b = outerIter.next();

                    CloseableIteration<BindingSet,QueryEvaluationException> innerIter = iterWrap(evaluationStrategy.evaluate(expr.getArg(), bindings));

                    boolean dominatesAll = true;

                    while (innerIter.hasNext()) {
                        BindingSet b2 = innerIter.next();
                        if (!b.getBinding(positionVarName).getValue().equals(b2.getBinding(positionVarName).getValue())) {

                            if (preferenceRelationEvaluator.isPreferred(expr.getPreference(), bindingSetUnWrap(b2), bindingSetUnWrap(b))) {
                                dominatesAll = false;
                            }
                        }
                    }
                    if (dominatesAll) {
                        return b;
                    }
                }
                return null;
            }
        });
    }

    @Override
    public boolean isApplicable(TupleExpr tupleExpr) {
        return true;
    }

    private CloseableIteration<BindingSet,QueryEvaluationException> iterWrap(CloseableIteration<BindingSet,QueryEvaluationException> iter) {
        return new LookAheadIteration<BindingSet, QueryEvaluationException>() {
            private int pos = 0;
            @Override
            protected BindingSet getNextElement() throws QueryEvaluationException {
                while (iter.hasNext()) {
                    return bindingSetWrap(iter.next(), pos++);
                }
                return null;
            }
        };
    }

    private CloseableIteration<BindingSet,QueryEvaluationException> iterUnWrap(CloseableIteration<BindingSet,QueryEvaluationException> iter) {
        return new LookAheadIteration<BindingSet, QueryEvaluationException>() {
            @Override
            protected BindingSet getNextElement() throws QueryEvaluationException {
                while (iter.hasNext()) {
                    return bindingSetUnWrap(iter.next());
                }
                return null;
            }
        };
    }

    private BindingSet bindingSetWrap(BindingSet b, int pos) {
        QueryBindingSet result = new QueryBindingSet(b);
        result.addBinding(positionVarName, SimpleValueFactory.getInstance().createLiteral(pos));
        return result;
    }

    private BindingSet bindingSetUnWrap(BindingSet b) {
        QueryBindingSet result = new QueryBindingSet(b);
        result.removeBinding(positionVarName);
        return result;
    }




}
