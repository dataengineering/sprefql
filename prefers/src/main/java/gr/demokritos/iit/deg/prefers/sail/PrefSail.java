package gr.demokritos.iit.deg.prefers.sail;

import gr.demokritos.iit.deg.prefers.evaluation.NLwinnowEvaluator;
import gr.demokritos.iit.deg.prefers.evaluation.Site;
import gr.demokritos.iit.deg.prefers.evaluation.WinnowEvaluator;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.SailConnection;
import org.eclipse.rdf4j.sail.SailException;
import org.eclipse.rdf4j.sail.helpers.SailWrapper;

/**
 * Created by antonis on 5/4/2017.
 */
public class PrefSail extends SailWrapper {


    private WinnowEvaluator winnowEvaluator;
    private Site site;
    private boolean fallbackToRewriteFlag;

    public PrefSail() {
        setWinnowEvaluator(new NLwinnowEvaluator());
    }

    public PrefSail(Sail base) {
        super(base);
        setWinnowEvaluator(new NLwinnowEvaluator());
    }

    @Override
    public SailConnection getConnection() throws SailException {
        return new PrefSailConnection(this, super.getConnection());
    }

    public void setWinnowEvaluator(WinnowEvaluator winnowEvaluator) {
        this.winnowEvaluator = winnowEvaluator;
    }

    public WinnowEvaluator getWinnowEvaluator() {
        return winnowEvaluator;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Site getSite() {
        return site;
    }

    public void setFallbackToRewrite(boolean fallbackToRewriteFlag) {
        this.fallbackToRewriteFlag = fallbackToRewriteFlag;
    }

    public boolean fallbackToRewrite() {
        return fallbackToRewriteFlag;
    }
}
