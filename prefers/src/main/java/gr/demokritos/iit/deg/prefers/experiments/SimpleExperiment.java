package gr.demokritos.iit.deg.prefers.experiments;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.Sail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by antonis on 6/4/2017.
 */
public class SimpleExperiment {

    private static final Logger logger = LoggerFactory.getLogger(SimpleExperiment.class);

    private String inputFile;
    private Sail baseSail;

    private Repository repository;

    private int queryResults;
    private long executionTime;

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public void setBaseSail(Sail baseSail) {
        this.baseSail = baseSail;
    }

    public int getLatestQueryResults() {
        return queryResults;
    }

    public long getLatestQueryExecutionTime() {
        return executionTime;
    }

    public void initialize() throws IOException {
        logger.info("Configuring repository");

        repository = new SailRepository(baseSail);
        repository.initialize();

        RepositoryConnection conn = repository.getConnection();

        logger.info("Importing statements in repository");

        Model model = Rio.parse(new FileInputStream(new File(inputFile)), "", RDFFormat.TURTLE);
        for (Statement statement: model) {
            conn.add(statement);
        }
        conn.close();
    }

    public void runQuery(String queryString) {

        RepositoryConnection conn = repository.getConnection();

        logger.info("Executing query");

        long start = System.currentTimeMillis();

        TupleQuery query = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = query.evaluate();

        queryResults=0;
        while (result.hasNext()) {
            logger.info(result.next().toString());
            queryResults++;
        }
        long end = System.currentTimeMillis();
        executionTime = end - start;

        logger.info("Found " + queryResults + " results");
        logger.info("Query Execution Time: " + executionTime + " ms");
    }
}
