package gr.demokritos.iit.deg.prefers.sail;

import gr.demokritos.iit.deg.prefers.commons.PrfSchema;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.sail.config.AbstractDelegatingSailImplConfig;
import org.eclipse.rdf4j.sail.config.SailConfigException;
import org.eclipse.rdf4j.sail.config.SailImplConfig;

/**
 * Created by antonis on 4/4/2017.
 */
public class PrefSailImplConfig extends AbstractDelegatingSailImplConfig {

    private int defaultWindowSize = 1024;
    private int windowSize = defaultWindowSize;

    private WinnowType winnowType = WinnowType.NL;
    private SiteType siteType = SiteType.LOCAL;
    private String endpointUrl = "";
    private Boolean fallbackToRewrite = false;

    public enum WinnowType {
        NL,
        BNL,
        CG,
        RW
    }

    public enum SiteType {
        REMOTE,
        LOCAL
    }

    public PrefSailImplConfig() {
        super(PrefSailFactory.SAIL_TYPE);
    }

    public PrefSailImplConfig(SailImplConfig delegate){
        super(PrefSailFactory.SAIL_TYPE, delegate);
    }

    @Override
    public void parse(Model m, Resource implNode) {
        super.parse(m, implNode);

        for (Value o : m.filter(implNode, PrfSchema.WINNOW_TYPE, null).objects()) {
            if (o.stringValue().equals("NL")) {
                winnowType = WinnowType.NL;
            }
            if (o.stringValue().equals("BNL")) {
                winnowType = WinnowType.BNL;
            }
            if (o.stringValue().equals("CG")) {
                winnowType = WinnowType.CG;
            }
            if (o.stringValue().equals("RW")) {
                winnowType = WinnowType.RW;
                fallbackToRewrite = true;
            }
        }

        for (Value o : m.filter(implNode, PrfSchema.WINDOW_SIZE, null).objects()) {
            try {
                windowSize = Integer.parseInt(o.stringValue());
            } catch (NumberFormatException e) {
                // do nothing
            }
        }

        for (Value o : m.filter(implNode, PrfSchema.SITE_TYPE, null).objects()) {
            if (o.stringValue().equals("REMOTE")) {
                siteType = SiteType.REMOTE;
            }
            if (o.stringValue().equals("LOCAL")) {
                siteType = SiteType.LOCAL;
            }
        }

        for (Value o : m.filter(implNode, PrfSchema.ENDPOINT_URL, null).objects()) {
            endpointUrl = o.stringValue();
        }

        for (Value o : m.filter(implNode, PrfSchema.FALLBACK_TO_REWRITE, null).objects()) {
            if (o.equals(SimpleValueFactory.getInstance().createLiteral(true))) {
                fallbackToRewrite = true;
            }
        }
    }

    @Override
    public void validate() throws SailConfigException {

        super.validate();

        // validate custom properties
    }

    @Override
    public Resource export(Model m) {
        Resource implNode = super.export(m);

        ValueFactory vf = SimpleValueFactory.getInstance();

        if (winnowType == WinnowType.NL) {
            m.add(implNode, PrfSchema.WINNOW_TYPE, vf.createLiteral("NL"));
        }

        if (winnowType == WinnowType.BNL) {
            m.add(implNode, PrfSchema.WINNOW_TYPE, vf.createLiteral("BNL"));
            m.add(implNode, PrfSchema.WINDOW_SIZE, vf.createLiteral(String.valueOf(windowSize)));
        }

        if (winnowType == WinnowType.CG) {
            m.add(implNode, PrfSchema.WINNOW_TYPE, vf.createLiteral("CG"));
        }

        if (winnowType == WinnowType.RW) {
            m.add(implNode, PrfSchema.WINNOW_TYPE, vf.createLiteral("RW"));
        }

        if (siteType == SiteType.LOCAL) {
            m.add(implNode, PrfSchema.SITE_TYPE, vf.createLiteral("LOCAL"));
        }

        if (siteType == SiteType.REMOTE) {
            m.add(implNode, PrfSchema.SITE_TYPE, vf.createLiteral("REMOTE"));
            m.add(implNode, PrfSchema.ENDPOINT_URL, vf.createLiteral(endpointUrl));
        }

        m.add(implNode, PrfSchema.FALLBACK_TO_REWRITE, vf.createLiteral(fallbackToRewrite));

        return implNode;
    }

    public int getWindowSize() { return windowSize; }

    public WinnowType getWinnowType() { return winnowType; }

    public SiteType getSiteType() {
        return siteType;
    }

    public String getEndpointUrl() {
        return endpointUrl;
    }

    public boolean fallbackToRewrite() {
        return fallbackToRewrite;
    }
}
