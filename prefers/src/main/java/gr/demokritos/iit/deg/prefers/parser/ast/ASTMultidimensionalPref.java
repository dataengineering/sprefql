/* Generated By:JJTree: Do not edit this line. ASTMultidimensionalPref.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package gr.demokritos.iit.deg.prefers.parser.ast;

public
class ASTMultidimensionalPref extends SimpleNode {
  public ASTMultidimensionalPref(int id) {
    super(id);
  }

  public ASTMultidimensionalPref(SyntaxTreeBuilder p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(SyntaxTreeBuilderVisitor visitor, Object data) throws VisitorException {
    return visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=59ef159b714770d9bbabefc8f1157838 (do not edit this line) */
