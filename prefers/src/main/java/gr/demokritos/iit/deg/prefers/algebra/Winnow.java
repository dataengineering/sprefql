package gr.demokritos.iit.deg.prefers.algebra;

import org.eclipse.rdf4j.query.algebra.*;

import java.util.List;

/**
 * Created by antonis on 20/3/2017.
 */
public class Winnow extends UnaryTupleOperator {

    private List<String> prefered;
    private List<String> nonPrefered;
    private List<String> base;

    private Preference preference;

    public Winnow(TupleExpr tupleExpr) {
        super(tupleExpr);
    }

    public Winnow(TupleExpr tupleExpr, List<String> prefered, List<String> nonPrefered, List<String> base, Preference preference) {
        super(tupleExpr);
        setPrefered(prefered);
        setNonPrefered(nonPrefered);
        setBase(base);
        setPreference(preference);
    }

    public void setBase(List<String> base) {
        this.base = base;
    }

    public void setPrefered(List<String> prefered) {
        this.prefered = prefered;
    }

    public void setNonPrefered(List<String> non_prefered) {
        this.nonPrefered = non_prefered;
    }

    public void setPreference(Preference condition) {
        assert condition != null : "condition must not be null";
        condition.setParentNode(this);
        this.preference = condition;
    }

    public List<String> getBase() {
        return base;
    }

    public List<String> getPrefered() {
        return prefered;
    }

    public List<String> getNonPrefered() {
        return nonPrefered;
    }

    public Preference getPreference() {
        return preference;
    }


    @Override
    public boolean equals(Object other) {
        if(other instanceof Winnow) {
            Winnow o = (Winnow)other;
            return (this.arg.equals(o.getArg()) &&
                    this.base.equals(o.getBase()) &&
                    this.prefered.equals(o.getPrefered()) &&
                    this.nonPrefered.equals(o.getNonPrefered()) &&
                    this.preference.equals(o.getPreference())
            );
        } else {
            return false;
        }
    }

    @Override
    public <X extends Exception> void visit(QueryModelVisitor<X> queryModelVisitor) throws X {
        queryModelVisitor.meetOther(this);
    }

    @Override
    public <X extends Exception> void visitChildren(QueryModelVisitor<X> visitor)
            throws X
    {
        preference.visit(visitor);
        super.visitChildren(visitor);
    }

    @Override
    public String getSignature() {
        StringBuilder sb = new StringBuilder(256);

        sb.append(super.getSignature());
        sb.append(" (").append(this.prefered.toString().replaceAll("\n",""));
        sb.append(" > (").append(this.nonPrefered.toString().replaceAll("\n",""));
        sb.append("), matching (").append(this.base.toString().replaceAll("\n",""));
        sb.append(")");

        return sb.toString();
    }

    @Override
    public Winnow clone() {
        Winnow clone = (Winnow)super.clone();
        clone.setBase(this.getBase());
        clone.setPrefered(this.getPrefered());
        clone.setNonPrefered(this.getNonPrefered());
        clone.setPreference(this.getPreference());
        return clone;
    }
}
