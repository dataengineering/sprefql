package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.Preference;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.common.iteration.LookAheadIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by antonis on 31/3/2017.
 */
public class BNLwindow {

    private static final Logger logger = LoggerFactory.getLogger(BNLwindow.class);

    private List<Pair<BindingSet, Integer>> windowList;
    private int windowSize;
    private int timeStamp;

    private Preference preferece;
    private PreferenceRelationEvaluator preferenceEvaluator;
    private CloseableIteration<BindingSet,QueryEvaluationException> input;


    public BNLwindow() {
        this.windowList = new ArrayList<>();
        this.timeStamp = 0;
    }

    public void refresh() {
        List<Pair<BindingSet, Integer>> newList = new ArrayList<>();
        for (Pair<BindingSet, Integer> pair: windowList) {
            newList.add(Pair.of(pair.getKey(),timeStamp));
        }
        windowList = newList;
        this.timeStamp = 0;
    }

    public void setPreferece(Preference preferece) {
        this.preferece = preferece;
    }

    public void setPreferenceEvaluator(PreferenceRelationEvaluator preferenceEvaluator) {
        this.preferenceEvaluator = preferenceEvaluator;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public void setInput(CloseableIteration<BindingSet, QueryEvaluationException> input) {
        this.input = input;
    }


    public CloseableIteration<BindingSet, QueryEvaluationException> overflowingBindingSets() {
        return (new LookAheadIteration<BindingSet, QueryEvaluationException>() {
            @Override
            protected BindingSet getNextElement() throws QueryEvaluationException {
                while (input.hasNext()) {
                    BindingSet bindingSet = input.next();
                    boolean insert = true;

                    ListIterator<Pair<BindingSet, Integer>> listIter = windowList.listIterator();
                    while (listIter.hasNext()) {
                        BindingSet curr = listIter.next().getKey();
                        if (preferenceEvaluator.isPreferred(preferece, bindingSet, curr)) {
                            listIter.remove();
                            logger.info("BNL window remove: window size = " + windowList.size());
                        } else if (preferenceEvaluator.isPreferred(preferece, curr, bindingSet)) {
                            insert = false;
                        }
                    }
                    if (insert) {
                        if (windowList.size() < windowSize) {
                            listIter.add(Pair.of(bindingSet, timeStamp));
                            logger.info("BNL window insert: window size = " + windowList.size());
                        } else {
                            timeStamp++;
                            return bindingSet;
                        }
                    }
                }
                return null;
            }
        });
    }

    public CloseableIteration<BindingSet, QueryEvaluationException> winningBindingSets() {
        return (new LookAheadIteration<BindingSet, QueryEvaluationException>() {

            private Iterator<Pair<BindingSet, Integer>> listIter = windowList.iterator();

            @Override
            protected BindingSet getNextElement() throws QueryEvaluationException {
                while (listIter.hasNext()) {
                    Pair<BindingSet, Integer> curr = listIter.next();
                    while (listIter.hasNext() && curr.getValue() != 0) {
                        curr = listIter.next();
                    }
                    listIter.remove();
                    logger.info("BNL window remove: window size = " + windowList.size());
                    return curr.getKey();
                }
                return null;
            }
        });
    }
}
