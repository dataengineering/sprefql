package gr.demokritos.iit.deg.prefers.sail;

import gr.demokritos.iit.deg.prefers.evaluation.PreferenceRelationEvaluator;
import gr.demokritos.iit.deg.prefers.evaluation.PreferentialEvaluationStrategy;
import gr.demokritos.iit.deg.prefers.evaluation.RemoteSiteEnforcer;
import gr.demokritos.iit.deg.prefers.evaluation.WinnowEvaluator;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import gr.demokritos.iit.deg.prefers.rewrite.PrefSparqlRewriter;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.*;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.TripleSource;
import org.eclipse.rdf4j.query.algebra.evaluation.federation.FederatedServiceResolver;
import org.eclipse.rdf4j.query.algebra.evaluation.federation.FederatedServiceResolverImpl;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.sail.SailConnection;
import org.eclipse.rdf4j.sail.SailException;
import org.eclipse.rdf4j.sail.evaluation.SailTripleSource;
import org.eclipse.rdf4j.sail.helpers.SailConnectionWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by antonis on 5/4/2017.
 */
public class PrefSailConnection extends SailConnectionWrapper {

    private static final Logger logger = LoggerFactory.getLogger(PrefSailConnection.class);

    private PrefSail baseSail;
    private FederatedServiceResolver resolver = new FederatedServiceResolverImpl();

    public PrefSailConnection(PrefSail sail, SailConnection baseConnection) {
        super(baseConnection);
        baseSail = sail;
    }

    @Override
    public void setFederatedServiceResolver(FederatedServiceResolver resolver) {
        this.resolver = resolver;
        super.setFederatedServiceResolver(resolver);
    }

    @Override
    public CloseableIteration<? extends BindingSet, QueryEvaluationException>
        evaluate(TupleExpr tupleExpr, Dataset dataset, BindingSet bindings, boolean includeInferred) throws SailException
    {
        TripleSource source = new SailTripleSource(super.getWrappedConnection(), includeInferred, this.baseSail.getValueFactory());

        if (baseSail.fallbackToRewrite()) {
            if (!(baseSail.getWinnowEvaluator().isApplicable(tupleExpr))) {
                if (baseSail.getSite().isRemote()) {
                    return evaluateRewriteRemote(tupleExpr, dataset, bindings, includeInferred);
                }
                else {
                    try {
                        PrefSparqlRewriter rewriter = new PrefSparqlRewriter();
                        PrefSPARQLParser parser = new PrefSPARQLParser();
                        String queryString = rewriter.rewrite(tupleExpr);
                        tupleExpr = parser.parseQuery(queryString, null).getTupleExpr();

                    } catch (Exception e) {
                        throw new QueryEvaluationException();
                    }
                }
            }
        }

        if (baseSail.getSite().isRemote()) {
            RemoteSiteEnforcer enforcer = new RemoteSiteEnforcer();
            enforcer.setSite(baseSail.getSite());
            tupleExpr = enforcer.enforceSite(tupleExpr);
        }

        logger.info("Executing plan");
        logger.info(tupleExpr.toString());

        PreferentialEvaluationStrategy strategy = new PreferentialEvaluationStrategy(source, resolver);
        PreferenceRelationEvaluator preferenceRelationEvaluator = new PreferenceRelationEvaluator(source, resolver);
        WinnowEvaluator evaluator = baseSail.getWinnowEvaluator();

        evaluator.setPreferenceRelationEvaluator(preferenceRelationEvaluator);
        evaluator.setEvaluationStrategy(strategy);
        strategy.setWinnowEvaluator(evaluator);
        try {
            return strategy.evaluate(tupleExpr, bindings);
        }catch (QueryEvaluationException e) {
            throw new SailException(e);
        }
    }

    private CloseableIteration<? extends BindingSet, QueryEvaluationException>
        evaluateRewriteRemote(TupleExpr tupleExpr, Dataset dataset, BindingSet bindings, boolean includeInferred) throws SailException
    {
        PrefSparqlRewriter rewriter = new PrefSparqlRewriter();
        try {
            String queryString = rewriter.rewrite(tupleExpr);
            Repository repository = new SPARQLRepository(baseSail.getSite().getEndpointUrl().toString());
            repository.initialize();
            RepositoryConnection connection = repository.getConnection();

            logger.info("Sending remote SPARQL query:" + queryString);

            TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
            return query.evaluate();

        } catch (Exception e) {
            throw new QueryEvaluationException();
        }
    }

}
