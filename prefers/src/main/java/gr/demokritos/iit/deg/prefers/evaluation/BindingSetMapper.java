package gr.demokritos.iit.deg.prefers.evaluation;

import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.UnaryTupleOperator;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;
import org.eclipse.rdf4j.query.impl.MapBindingSet;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by antonis on 28/3/2017.
 */
public class BindingSetMapper {

    private TupleExpr expr;
    private Map<String, String> preferMap = new HashMap<>();
    private Map<String, String> toMap = new HashMap<>();
    private int size;

    public BindingSetMapper(TupleExpr expr) {
        expr.visit(new PreferenceQueryVisitor());
    }

    public BindingSet get(BindingSet prefer, BindingSet to) {

        MapBindingSet bindings = new MapBindingSet();

        if (prefer.size() != size || to.size() != size) {
            throw new RuntimeException("Bindingsets with wrong sizes");
        }

        for (Binding b : prefer) {
            bindings.addBinding(preferMap.get(b.getName()), b.getValue());
        }
        for (Binding b : to) {
            bindings.addBinding(toMap.get(b.getName()), b.getValue());
        }
        return bindings;
    }

    private class PreferenceQueryVisitor extends AbstractQueryModelVisitor<RuntimeException> {

        @Override
        public void meetUnaryTupleOperator(UnaryTupleOperator node) {
            if (node instanceof Winnow)
                meet((Winnow)node);
            else
                super.meetUnaryTupleOperator(node);
        }

        public void meet(Winnow node) {
            size = node.getBase().size();

            if (node.getPrefered().size() != size) {
                throw new RuntimeException("Wrong number of left preference argument variable list");
            }
            if (node.getNonPrefered().size() != size) {
                throw new RuntimeException("Wrong number of right preference argument variable list");
            }

            for (int i=0; i<size; i++) {
                if (preferMap.get(node.getPrefered().get(i)) != null || toMap.get(node.getNonPrefered().get(i)) != null ) {
                    throw new RuntimeException("Non distinct variable list in preference arguments");
                }
                preferMap.put(node.getBase().get(i), node.getPrefered().get(i));
                toMap.put(node.getBase().get(i), node.getNonPrefered().get(i));
            }
        }
    }
}
