package gr.demokritos.iit.deg.prefers.evaluation;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by antonis on 20/4/2017.
 */
public class Site {

    private URL endpointUrl;

    public Site() {
        endpointUrl = null;
    }

    public Site(String endpointUrl) {
        try {
            this.endpointUrl = new URL(endpointUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            this.endpointUrl = null;
        }
    }

    public boolean isRemote() {
        return endpointUrl != null;
    }

    public URL getEndpointUrl() {
        return endpointUrl;
    }
}
