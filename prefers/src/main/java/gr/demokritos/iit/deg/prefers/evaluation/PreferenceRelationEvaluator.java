package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.*;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.TripleSource;
import org.eclipse.rdf4j.query.algebra.evaluation.federation.FederatedServiceResolver;
import org.eclipse.rdf4j.query.algebra.evaluation.impl.StrictEvaluationStrategy;

/**
 * Created by antonis on 28/3/2017.
 */
public class PreferenceRelationEvaluator extends StrictEvaluationStrategy {

    private BindingSetMapper mapper;

    public PreferenceRelationEvaluator(TripleSource tripleSource, FederatedServiceResolver serviceResolver) {
        super(tripleSource, serviceResolver);
    }

    public void setMapper(BindingSetMapper mapper) {
        this.mapper = mapper;
    }


    public boolean isPreferred(Preference preference, BindingSet prefer, BindingSet to) throws QueryEvaluationException {
        if (preference instanceof SimplePref) {
            return this.isPreferred((SimplePref) preference, prefer, to);
        }
        if (preference instanceof PrioritizedPref) {
            return this.isPreferred((PrioritizedPref) preference, prefer, to);
        }
        if (preference instanceof MultidimensionalPref) {
            return this.isPreferred((MultidimensionalPref) preference, prefer, to);
        }
        throw new RuntimeException("Illegal preference operation");
    }

    public boolean isPreferred(SimplePref preference, BindingSet prefer, BindingSet to) {
        BindingSet bindings = mapper.get(prefer, to);
        return isTrue(preference.getArg(), bindings);
    }

    public boolean isPreferred(PrioritizedPref preference, BindingSet prefer, BindingSet to) {

        Preference p1 = (Preference) preference.getLeftArg();
        Preference p2 = (Preference) preference.getRightArg();

        boolean a, b, c;

        if (a = isPreferred(p1, prefer, to)) {
            return true;
        } else {
            b = isPreferred(p1, to, prefer);
            c = isPreferred(p2, prefer, to);
            return (!a && !b && c);
        }
    }

    public boolean isPreferred(MultidimensionalPref preference, BindingSet prefer, BindingSet to) {

        Preference p1 = (Preference) preference.getLeftArg();
        Preference p2 = (Preference) preference.getRightArg();

        if (isPreferred(p1, prefer, to) && (!isPreferred(p2, to, prefer))) {
            return true;
        } else {
            return (isPreferred(p2, prefer, to) && (!isPreferred(p1, to, prefer)));
        }
    }

    public CloseableIteration<BindingSet, QueryEvaluationException> evaluate(TupleExpr expr,
                                                                             BindingSet bindings)
            throws QueryEvaluationException {
        if (expr instanceof RemoteQuery) {
            return evaluate((RemoteQuery) expr, bindings);
        }
        return super.evaluate(expr, bindings);
    }

    public CloseableIteration<BindingSet, QueryEvaluationException> evaluate(RemoteQuery expr,
                                                                             BindingSet bindings) {
        return new RemoteQueryExecutor().evaluate(expr, bindings);
    }
}

