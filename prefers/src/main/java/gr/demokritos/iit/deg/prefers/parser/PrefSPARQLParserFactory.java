/*******************************************************************************
 * Copyright (c) 2015 Eclipse RDF4J contributors, Aduna, and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Distribution License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *******************************************************************************/
package gr.demokritos.iit.deg.prefers.parser;

import gr.demokritos.iit.deg.prefers.commons.Language;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.parser.QueryParser;
import org.eclipse.rdf4j.query.parser.QueryParserFactory;

/**
 * A {@link QueryParserFactory} for SPARQL parsers.
 * 
 * @author Arjohn Kampman
 */
public class PrefSPARQLParserFactory implements QueryParserFactory {

	private final PrefSPARQLParser singleton = new PrefSPARQLParser();

	/**
	 * Returns {@link QueryLanguage#SPARQL}.
	 */
	public QueryLanguage getQueryLanguage() {
		return Language.PrefSPARQL;
	}

	/**
	 * Returns a shared, thread-safe, instance of SPARQLParser.
	 */
	public QueryParser getParser() {
		return singleton;
	}
}
