package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.common.iteration.LookAheadIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.EvaluationStrategy;

/**
 * Created by antonis on 30/3/2017.
 */
public class BNLwinnowEvaluator implements WinnowEvaluator {

    private PreferenceRelationEvaluator preferenceRelationEvaluator;
    private EvaluationStrategy evaluationStrategy;
    private int windowSize;

    public void setPreferenceRelationEvaluator(PreferenceRelationEvaluator preferenceRelationEvaluator) {
        this.preferenceRelationEvaluator = preferenceRelationEvaluator;
    }

    public void setEvaluationStrategy(EvaluationStrategy evaluationStrategy) {
        this.evaluationStrategy = evaluationStrategy;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public CloseableIteration<BindingSet,QueryEvaluationException> evaluate(Winnow expr, BindingSet bindings) {

        BindingSetMapper mapper = new BindingSetMapper(expr);
        preferenceRelationEvaluator.setMapper(mapper);

        CloseableIteration<BindingSet,QueryEvaluationException> innerIteration = evaluationStrategy.evaluate(expr.getArg(), bindings);

        BNLwindow window = new BNLwindow();
        window.setWindowSize(windowSize);
        window.setPreferece(expr.getPreference());
        window.setPreferenceEvaluator(preferenceRelationEvaluator);

        window.setInput(innerIteration);
        BNLtempFile tempFile1 = new BNLtempFile();
        tempFile1.fillWith(window.overflowingBindingSets());

        CloseableIteration<BindingSet,QueryEvaluationException> windowIter1 = window.winningBindingSets();

        return (new LookAheadIteration<BindingSet, QueryEvaluationException>() {
            private BNLtempFile tempFile = tempFile1;
            private CloseableIteration<BindingSet,QueryEvaluationException> windowIter = windowIter1;
            private boolean flag = true;

            private void initializeNewPhase() {
                window.refresh();
                window.setInput(tempFile.scan());
                tempFile = new BNLtempFile();
                tempFile.fillWith(window.overflowingBindingSets());
                windowIter = window.winningBindingSets();
            }

            private boolean hasRemainingSolutions() {
                boolean cond = (windowIter.hasNext() || tempFile.scan().hasNext());
                if (cond) {
                    return cond;
                }
                else {
                    /* there exists a case, which happens only once, in which the file is empty, the iteration is empty
                     * but the window contains elements. We handle this situation */
                    if (flag) {
                        flag = false;
                        return true;
                    }
                    else {
                        return cond;
                    }
                }
            }

            @Override
            protected BindingSet getNextElement() throws QueryEvaluationException {

                while (hasRemainingSolutions()) {
                    if (!windowIter.hasNext()) {
                        initializeNewPhase();
                    }
                    if (windowIter.hasNext()) {
                        return (windowIter.next());
                    }
                }
                return null;
            }
        });
    }

    @Override
    public boolean isApplicable(TupleExpr tupleExpr) {
        try {
            BNLchecker checker = new BNLchecker(tupleExpr);
            return checker.hasTransitivePreference();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
