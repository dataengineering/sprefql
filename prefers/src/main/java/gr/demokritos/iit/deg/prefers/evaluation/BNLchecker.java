package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.Preference;
import gr.demokritos.iit.deg.prefers.algebra.SimplePref;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.query.algebra.*;

import java.util.Arrays;

/**
 * Created by antonis on 13/7/2017.
 */
public class BNLchecker {

    private Preference preference;

    private Compare.CompareOp ops[] = {
            Compare.CompareOp.EQ,
            Compare.CompareOp.GE,
            Compare.CompareOp.GT,
            Compare.CompareOp.LE,
            Compare.CompareOp.LT,
    };

    public BNLchecker(TupleExpr expr) {
        Winnow winnow = TransitivityChecker.GetWinnowVisitor.getWinnow(expr);
        preference = winnow.getPreference();
    }

   public boolean hasTransitivePreference() {
        return traverse(preference);
    }

    private boolean traverse(ValueExpr expr) {
        if (expr instanceof Compare) {
            return traverseCompare((Compare) expr);
        }
        if (expr instanceof SimplePref) {
            return traverse(((SimplePref) expr).getArg());
        }
        if (expr instanceof And) {
            return traverse(((And) expr).getLeftArg()) && traverse(((And) expr).getRightArg());
        }
        return false;
    }

    private boolean traverseCompare(Compare expr) {
        return (Arrays.asList(ops).contains(expr.getOperator()) &&
                varOrValue(expr.getLeftArg()) && varOrValue(expr.getRightArg()));
    }

    private boolean varOrValue(ValueExpr expr) {
        return (expr instanceof Var || expr instanceof ValueConstant);
    }


}
