package gr.demokritos.iit.deg.prefers.experiments;

/**
 * Created by antonis on 6/4/2017.
 */
public final class QueriesOld {

    public final static String booksPrefQuery = "" +
            "PREFIX ex: <http://example.org/>" +
            "SELECT ?isbn ?vendor ?price\n" +
            "WHERE {\n" +
            "  ?s ex:isbn ?isbn .\n" +
            "  ?s ex:vendor ?vendor .\n" +
            "  ?s ex:price ?price .\n" +
            "}\n" +
            "PREFER (?i1 ?v1 ?p1) TO (?i2 ?v2 ?p2)\n" +
            "IF (?i1 = ?i2 && ?p1 < ?p2)";

    public final static String booksPrefQuery1 = "" +
            "PREFIX ex: <http://example.org/>" +
            "SELECT ?isbn ?vendor ?price\n" +
            "WHERE {\n" +
            "  ?s ex:isbn ?isbn .\n" +
            "  ?s ex:vendor ?vendor .\n" +
            "  ?s ex:price ?price .\n" +
            "}\n" +
            "PREFER (?i1 ?v1 ?p1) TO (?i2 ?v2 ?p2)\n" +
            "IF (?i1 = ?i2) PRIOR TO (?p1 < ?p2)";

    public final static String booksPrefQuery3 = "" +
            "PREFIX ex: <http://example.org/>\n" +
            "SELECT ?isbn (sum(?price) as ?totalprice)\n" +
            "WHERE {\n" +
            "  ?s ex:isbn ?isbn .\n" +
            "  ?s ex:price ?price .\n" +
            "}\n" +
            "GROUP BY ?isbn \n" +
            "PREFER (?i1 ?p1) TO (?i2 ?p2)\n" +
            "IF (?p1 < ?p2)";

    public final static String booksPrefQuery2 = "" +
            "PREFIX ex: <http://example.org/>\n" +
            "SELECT ?isbn ?rating ?price\n" +
            "WHERE {\n" +
            "  ?s ex:isbn ?isbn .\n" +
            "  ?s ex:rating ?rating .\n" +
            "  ?s ex:price ?price .\n" +
            "}\n" +
            "PREFER (?i1 ?r1 ?p1) TO (?i2 ?r2 ?p2)\n" +
            "IF (?r1 > 150 && ?r2 > 150 && ?p1 > ?p2 || ?r1 < 150 && ?r2 < 150 && ?p1 < ?p2)" +
            "PRIOR TO (?r1 > 150 && ?r2 < 150)" +
            "";

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* I want to watch a movie in the "lang" language. I prefer a movie solely spoken in "lang". */

    public final static String lang = "el"; /* el for greek, eo for esperanto */

    public final static String singleLangMoviesQuery = "" +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "PREFIX lingvo: <http://www.lingvoj.org/lingvo/>\n" +
            "SELECT ?director ?movie (count(?l) as ?languages)\n" +
            "WHERE {\n" +
            "  ?d a movie:director .\n" +
            "  ?d rdfs:label ?director .\n" +
            "  ?f movie:director ?d .\n" +
            "  ?f movie:language lingvo:" + lang + " .\n" +
            "  ?f movie:language ?l .\n" +
            "  ?f rdfs:label ?movie .\n" +
            "  ?f movie:runtime ?runtume\n" +
            "} GROUP BY ?director ?movie\n" +
            "PREFER (?d1 ?m1 ?l1) TO (?d2 ?m2 ?l2)\n" +
            "IF (?l1 = 1 && ?l2 > 1)";

    /* I want to watch a movie in the "lang" language. I prefer a movie solely spoken in "lang". */

    public final static String singleLangMoviesQuery2 = "" +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "PREFIX lingvo: <http://www.lingvoj.org/lingvo/>\n" +
            "SELECT ?a\n" +
            "WHERE {\n" +
            "  ?a a movie:film .\n" +
            "  ?a movie:language lingvo:" + lang + " .\n" +
            "}\n" +
            "PREFER ?m1 TO ?m2 \n" +
            "IF NOT EXISTS { ?m1 movie:language ?l. FILTER (?m1 != lingvo:" + lang + ") } ";

    /* I want to watch an Action movie. */

    public final static String actionMovieQueryBody = "" +
            "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
            "PREFIX dc: <http://purl.org/dc/terms/>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "SELECT ?title (xsd:float(?runtimeStr) as ?runtime) (xsd:int(substr(?dateStr,1,4)) as ?year) \n" +
            "WHERE {\n" +
            "  ?a a movie:film .\n" +
            //"  ?a movie:genre ?g .\n" +
            //"  ?g movie:film_genre_name \"Action\" .\n" +
            "  ?a movie:runtime ?runtimeStr.\n" +
            "  ?a dc:title ?title.\n" +
            "  ?a dc:date ?dateStr.\n" +
            "}\n";

    /* Prefer the oldest */

    public final static String oldMovieQuery = actionMovieQueryBody +
            "PREFER (?t1 ?r1 ?y1) TO (?t2 ?r2 ?y2) IF (?y1 < ?y2)";

    /* Prefer the longest */

    public final static String longMovieQuery = actionMovieQueryBody +
            "PREFER (?t1 ?r1 ?y1) TO (?t2 ?r2 ?y2) IF (?r1 > ?r2)";

    /* Prefer both long and new movies with equal importance. */

    public final static String longNewMovieQuery = actionMovieQueryBody +
            "PREFER (?t1 ?r1 ?y1) TO (?t2 ?r2 ?y2)\n" +
            "IF (?r1 > ?r2) AND (?y1 > ?y2)";

    /* Prefer a movie within a specific runtime */

    public final static String runtime_from = "115";
    public final static String runtime_to = "125";

    public final static String betweenMovieQuery = actionMovieQueryBody +
            "PREFER (?t1 ?r1 ?y1) TO (?t2 ?r2 ?y2)\n" +
            "IF (?r1 >= " + runtime_from + " && ?r1 <= " + runtime_to + " " +
            "&& (?r2 < "  + runtime_from + " || ?r2 > "  + runtime_to + "))";

    /* Prefer a movie within a specific runtime and then prefer latest ones. */

    public final static String year = "2005";

    public final static String betweenLatestMovieQuery = actionMovieQueryBody +
            "PREFER (?t1 ?r1 ?y1) TO (?t2 ?r2 ?y2)\n" +
            "IF (?r1 >= " + runtime_from + " && ?r1 <= " + runtime_to + " " +
            "&& (?r2 < "  + runtime_from + " || ?r2 > "  + runtime_to + ")) " +
            "PRIOR TO (?y1 >= " + year + " && ?y2 < " + year + ")";

    /* Prefer long movies before a specific year and short movies after this year */

    public final static String longShortMovieQuery = actionMovieQueryBody +
            "PREFER (?t1 ?r1 ?y1) TO (?t2 ?r2 ?y2)\n" +
            "IF (?y1 >= " + year + " && ?y2 >= " + year + " && ?r1 < ?r2" +
            " || ?y1 < "  + year + " && ?y2 < "  + year + " && ?r1 > ?r2)";

    /* Prefer a movie from its sequel. */

    public final static String vanillaSequelMovieQuery1 =
            "PREFIX dc: <http://purl.org/dc/terms/>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "SELECT ?film ?title\n" +
            "WHERE {\n" +
            "  ?film a movie:film .\n" +
            "  ?film dc:title ?title.\n" +
            //"  filter regex(?title, \"Mad Max\").\n" +
            "}\n";

    public final static String sequelMovieQuery1 = vanillaSequelMovieQuery1 +
            "PREFER (?f1 ?t1) TO (?f2 ?t2)\n" +
            "IF EXISTS { ?f1 movie:sequel ?f2 }"
            ;

    public final static String sequelMovieQuery2 =
            "PREFIX dc: <http://purl.org/dc/terms/>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "SELECT ?film ?title\n" +
            "WHERE {\n" +
            "  ?film a movie:film.\n" +
            "  ?film dc:title ?title.\n" +
            "  ?film movie:performance ?performance.\n" +
            "  ?performance movie:performance_character \"Han Solo\".\n" +
            "}\n" +
            "PREFER (?f1 ?t1) TO (?f2 ?t2)\n" +
            "IF EXISTS { ?f1 movie:sequel+ ?f2 }"
            ;

    public final static String recentSequelMovieQuery =
            "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
            "PREFIX dc: <http://purl.org/dc/terms/>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "SELECT DISTINCT ?film ?title (xsd:int(substr(?dateStr,1,4)) as ?year)\n" +
            "WHERE {\n" +
            "  ?film a movie:film .\n" +
            "  ?film dc:title ?title.\n" +
            "  ?film movie:genre ?g .\n"
            + "  ?film movie:runtime ?runtimeStr.\n"
            + "  ?film dc:date ?dateStr.\n"
            + "  ?g movie:film_genre_name \"Action\" .\n"
            + "FILTER regex(?title, \"Max\")\n"
            + "}\n"
            + "PREFER (?f1 ?t1 ?y1) TO (?f2 ?t2 ?y2)\n"
            //+ "IF EXISTS { ?f1 movie:sequel ?f2 } PRIOR TO (?y1 > 1980)"
            //+ "IF (?y1 > 1980) PRIOR TO EXISTS { ?f1 movie:sequel ?f2 }"
            //+ "IF (?y2 < 1980) PRIOR TO EXISTS { ?f1 movie:sequel ?f2 }"
            //+ "IF (?y1 > ?y2 + 5) PRIOR TO EXISTS { ?f1 movie:sequel ?f2 }"
            + "IF (?y1 > ?y2 + 5 && EXISTS { ?f1 movie:sequel ?f2 })"
            ;

    public final static String newquery =
            "PREFIX dc: <http://purl.org/dc/terms/>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "SELECT ?title ?genre (xsd:float(?runtimeStr) as ?runtime) \n" +
            "WHERE {\n" +
            "  ?s a movie:film.\n" +
            "  ?s dc:title ?title.\n" +
            "  ?s movie:genre ?g .\n" +
            "  ?g movie:film_genre_name ?genre .\n" +
            "  ?s movie:runtime ?runtimeStr.\n" +
            "}\n" +
            "PREFER (?i1 ?v1 ?p1) TO (?i2 ?v2 ?p2)\n" +
            "IF (?v1 = ?v2 && ?p1 > ?p2)";

}