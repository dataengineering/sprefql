package gr.demokritos.iit.deg.prefers.experiments;

import gr.demokritos.iit.deg.prefers.commons.Language;
import gr.demokritos.iit.deg.prefers.sail.PrefSailImplConfig;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.config.RepositoryFactory;
import org.eclipse.rdf4j.repository.config.RepositoryRegistry;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by antonis on 6/4/2017.
 */
public class LinkedMDBExperiment extends BooksExperiment {

    private static final Logger logger = LoggerFactory.getLogger(LinkedMDBExperiment.class);

    private static String all[] = {
            Queries.Q1,
            Queries.Q2,
            Queries.Q3,
            Queries.Q4,
            Queries.Q5,
            Queries.Q6,
            Queries.Q7,
    };

    private static String rewrited[] = {
            Rewrites.Q1,
            Rewrites.Q2,
            Rewrites.Q3,
            Rewrites.Q4,
            Rewrites.Q5,
            Rewrites.Q6,
            Rewrites.Q7,
    };

    private static String inners[] = {
            Queries.B1,
            Queries.B2,
            Queries.B4,
            Queries.B5,
            Queries.B7,
    };


    public static void main(String[] args) throws Exception {
        clearTmp();
        if (args.length < 4) {
            logger.info("Usage: dump [nl-config] [bnl-config] [path-to-dump-file]");
            logger.info("   or: remote [nl-config] [bnl-config] [endpoint]");
        }
        else {
            String nlconfig = args[1];
            String bnlconfig = args[2];
            String endpoint = "";
            String dumpfile = "";

            if (args[0].equals("dump")) {
                dumpfile = args[3];
            }
            if (args[0].equals("remote")) {
                endpoint = args[3];
            }

            for (String preference_query : inners) {
                runExperimentSimple("IN", endpoint, dumpfile, preference_query);
            }
            for (String preference_query : all) {
                runExperiment("NL", nlconfig, dumpfile, preference_query);
            }
            for (String preference_query : rewrited) {
                runExperimentSimple("RW", endpoint, dumpfile, preference_query);
            }
            for (String preference_query : all) {
                runExperiment("BNL", bnlconfig, dumpfile, preference_query);
            }
        }
    }

    public static void runExperiment(String id, String configFile, String dumpFile, String queryString) throws IOException {

        Model configModel = Rio.parse(new FileInputStream(new File(configFile)), "", RDFFormat.TURTLE);

        SailRepositoryConfig config = new SailRepositoryConfig();

        MemoryStoreConfig memoryStoreConfig = new MemoryStoreConfig();
        PrefSailImplConfig prefSailConfig = new PrefSailImplConfig(memoryStoreConfig);
        prefSailConfig.parse(configModel, null);
        config.setSailImplConfig(prefSailConfig);

        Optional<RepositoryFactory> factory = RepositoryRegistry.getInstance().get(config.getType());

        if (!factory.isPresent()) {
            throw new IOException("Factory not present!");
        }

        Repository repository = factory.get().getRepository(config);
        repository.initialize();

        RepositoryConnection conn = repository.getConnection();

        if (!dumpFile.equals("")) {
            importDumpFile(conn,dumpFile);
        }

        logger.info("Executing query");
        logger.info(queryString);

        long start = System.currentTimeMillis();

        TupleQuery q = conn.prepareTupleQuery(Language.PrefSPARQL, queryString);
        TupleQueryResult result = q.evaluate();

        int queryResults = 0;

        while (result.hasNext()) {
            BindingSet b = result.next();
            logger.info(b.toString());
            queryResults++;
        }
        conn.close();

        long end = System.currentTimeMillis();
        long executionTime = end - start;

        logger.info("Found " + queryResults + " results");
        logger.info("Query Execution Time: " + executionTime + " ms");

        writeResults("/tmp/results.csv", id, executionTime, queryResults);
    }

    public static void runExperimentSimple(String id, String endpoint, String dumpFile, String queryString) throws IOException {

        Repository repository;

        if (!endpoint.equals("")) {
            repository = new SPARQLRepository(endpoint);
            repository.initialize();
        }
        else {
            repository = new SailRepository(new MemoryStore());
            repository.initialize();
        }

        RepositoryConnection conn = repository.getConnection();

        if (!dumpFile.equals("")) {
            importDumpFile(conn,dumpFile);
        }

        logger.info("Executing query");
        logger.info(queryString);

        long start = System.currentTimeMillis();

        TupleQuery q = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = q.evaluate();

        int queryResults = 0;

        while (result.hasNext()) {
            BindingSet b = result.next();
            logger.info(b.toString());
            queryResults++;
        }
        conn.close();

        long end = System.currentTimeMillis();
        long executionTime = end - start;

        logger.info("Found " + queryResults + " results");
        logger.info("Query Execution Time: " + executionTime + " ms");

        writeResults("/tmp/results.csv", id, executionTime, queryResults);
    }

    public static void importDumpFile(RepositoryConnection conn, String dumpFile) throws IOException {

        logger.info("Importing statements in repository");

        Model model = Rio.parse(new FileInputStream(new File(dumpFile)), "", RDFFormat.TURTLE);
        for (Statement statement: model) {
            conn.add(statement);
        }
    }
}
