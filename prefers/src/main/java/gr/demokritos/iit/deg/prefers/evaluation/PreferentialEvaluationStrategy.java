package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.RemoteQuery;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.TripleSource;
import org.eclipse.rdf4j.query.algebra.evaluation.federation.FederatedServiceResolver;
import org.eclipse.rdf4j.query.algebra.evaluation.impl.StrictEvaluationStrategy;

/**
 * Created by antonis on 3/4/2017.
 */
public class PreferentialEvaluationStrategy extends StrictEvaluationStrategy {

    private WinnowEvaluator winnowEvaluator;

    public PreferentialEvaluationStrategy(TripleSource tripleSource, FederatedServiceResolver serviceResolver) {
        super(tripleSource, serviceResolver);
    }

    public void setWinnowEvaluator(WinnowEvaluator winnowEvaluator) {
        this.winnowEvaluator = winnowEvaluator;
    }

    public CloseableIteration<BindingSet, QueryEvaluationException> evaluate(TupleExpr expr,
                                                                             BindingSet bindings)
            throws QueryEvaluationException {
        if (expr instanceof Winnow) {
            return winnowEvaluator.evaluate((Winnow) expr, bindings);
        }
        if (expr instanceof RemoteQuery) {
            return evaluate((RemoteQuery) expr, bindings);
        }
        return super.evaluate(expr,bindings);
    }

    public CloseableIteration<BindingSet, QueryEvaluationException> evaluate(RemoteQuery expr,
                                                                             BindingSet bindings) {
        try {
            return new RemoteQueryExecutor().evaluate(expr, bindings);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
