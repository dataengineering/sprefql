package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.RemoteQuery;
import gr.demokritos.iit.deg.prefers.rewrite.SPARQLQueryRenderer;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.common.iteration.EmptyIteration;
import org.eclipse.rdf4j.common.iteration.SingletonIteration;
import org.eclipse.rdf4j.query.*;
import org.eclipse.rdf4j.query.algebra.Var;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;
import org.eclipse.rdf4j.query.parser.ParsedBooleanQuery;
import org.eclipse.rdf4j.query.parser.ParsedTupleQuery;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by antonis on 24/4/2017.
 */
public class RemoteQueryExecutor {

    private static final Logger logger = LoggerFactory.getLogger(RemoteQueryExecutor.class);

    public CloseableIteration<BindingSet, QueryEvaluationException> evaluate(RemoteQuery expr,
                                                                             BindingSet bindings) {

        try {
            final Set<String> vars = new HashSet<>();

            expr.visit(new AbstractQueryModelVisitor<Exception>() {
                @Override
                public void meet(Var node) throws Exception {
                    if (bindings.getBindingNames().contains(node.getName())) {
                        node.setValue(bindings.getValue(node.getName()));
                    }
                    else {
                        vars.add(node.getName());
                    }
                }
            });

            SPARQLQueryRenderer renderer = new SPARQLQueryRenderer();

            if (!vars.isEmpty()) {
                ParsedTupleQuery tupleQuery = new ParsedTupleQuery(expr.getArg());
                String queryString = removeStringDatatypes(renderer.render(tupleQuery));
                String endpointUrl = expr.getSite().getEndpointUrl().toString();

                Repository repository = new SPARQLRepository(endpointUrl);
                repository.initialize();
                RepositoryConnection connection = repository.getConnection();

                logger.info("Sending remote SPARQL query:" + queryString);

                TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
                return query.evaluate();
            }
            else {
                ParsedBooleanQuery tupleQuery = new ParsedBooleanQuery(expr.getArg());
                String queryString = removeStringDatatypes(renderer.render(tupleQuery));
                String endpointUrl = expr.getSite().getEndpointUrl().toString();

                Repository repository = new SPARQLRepository(endpointUrl);
                repository.initialize();
                RepositoryConnection connection = repository.getConnection();

                logger.info("Sending remote SPARQL query:" + queryString);

                BooleanQuery query = connection.prepareBooleanQuery(QueryLanguage.SPARQL, queryString);
                if (query.evaluate() == true) {
                    return new SingletonIteration<>(bindings);
                }
                else {
                    return new EmptyIteration<>();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String removeStringDatatypes(String queryStr) {
        return queryStr.replaceAll("\"\"\"(.+)\"\"\"\\^\\^<http://www.w3.org/2001/XMLSchema#string>","\"$1\"");
    }
}
