package gr.demokritos.iit.deg.prefers.algebra;

import org.eclipse.rdf4j.query.algebra.QueryModelVisitor;
import org.eclipse.rdf4j.query.algebra.UnaryValueOperator;
import org.eclipse.rdf4j.query.algebra.ValueExpr;

/**
 * Created by antonis on 20/3/2017.
 */
public class SimplePref extends UnaryValueOperator implements Preference {

    public SimplePref(ValueExpr arg) {
        setArg(arg);
    }

    @Override
    public <X extends Exception> void visit(QueryModelVisitor<X> queryModelVisitor) throws X {
        queryModelVisitor.meetOther(this);
    }
}
