package gr.demokritos.iit.deg.prefers.rewrite;

import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.algebra.*;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.ParsedTupleQuery;
import org.eclipse.rdf4j.queryrender.QueryRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tru on 3/7/2017.
 */
public class SPARQLnotExistsQueryRenderer implements QueryRenderer {

    private SPARQLQueryRenderer sparqlQueryRenderer = new SPARQLQueryRenderer();

    @Override
    public QueryLanguage getLanguage() {
        return QueryLanguage.SPARQL;
    }

    @Override
    public String render(ParsedQuery parsedQuery) throws Exception {
        TupleExpr expr = parsedQuery.getTupleExpr().clone();
        List<TupleExpr> subqueryList = new ArrayList<>();

        expr.visit(new AbstractQueryModelVisitor<RuntimeException>() {

            private boolean isInnerProjection(Projection node) {
                QueryModelNode tmp = node.getParentNode();

                while (tmp != null) {
                    if (tmp instanceof Projection) {
                        return true;
                    }
                    tmp = tmp.getParentNode();
                }
                return false;
            }

            private StatementPattern createStatement(int i) {
                Var var = new Var("x" + i);
                return new StatementPattern(var,var,var);
            }

            @Override
            public void meet(Projection node) throws RuntimeException {
                super.meet(node);
                if (isInnerProjection(node)) {
                    int sz = subqueryList.size();
                    subqueryList.add(node.clone());
                    node.replaceWith(createStatement(sz));
                }
            }
        });

        String subQuery0 = sparqlQueryRenderer.render(new ParsedTupleQuery(subqueryList.get(0)));
        String subQuery1 = sparqlQueryRenderer.render(new ParsedTupleQuery(subqueryList.get(1)));

        subQuery0 = subQuery0.replace("\n","\n\t");
        subQuery1 = subQuery1.replace("\n","\n\t");

        String queryStr = sparqlQueryRenderer.render(new ParsedTupleQuery(expr));

        queryStr = queryStr.replace("?x0 ?x0 ?x0.", "{\n\t" + subQuery0 + "}");
        queryStr = queryStr.replace("?x1 ?x1 ?x1.", "{\n\t" + subQuery1 + "}");

        return queryStr;
    }


}
