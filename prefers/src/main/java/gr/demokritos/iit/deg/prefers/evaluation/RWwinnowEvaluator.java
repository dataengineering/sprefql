package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.EvaluationStrategy;

/**
 * Created by antonis on 4/7/2017.
 */
public class RWwinnowEvaluator implements WinnowEvaluator {

    // dummy winnow evaluator that always falls back to rewrite.

    @Override
    public void setPreferenceRelationEvaluator(PreferenceRelationEvaluator preferenceRelationEvaluator) {

    }

    @Override
    public void setEvaluationStrategy(EvaluationStrategy evaluationStrategy) {

    }

    @Override
    public CloseableIteration<BindingSet, QueryEvaluationException> evaluate(Winnow expr, BindingSet bindings) {
        return null;
    }

    @Override
    public boolean isApplicable(TupleExpr tupleExpr) {
        return false;
    }
}
