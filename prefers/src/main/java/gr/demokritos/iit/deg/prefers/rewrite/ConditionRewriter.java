package gr.demokritos.iit.deg.prefers.rewrite;

import gr.demokritos.iit.deg.prefers.algebra.MultidimensionalPref;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.query.algebra.And;
import org.eclipse.rdf4j.query.algebra.Not;
import org.eclipse.rdf4j.query.algebra.Or;
import org.eclipse.rdf4j.query.algebra.ValueExpr;
import gr.demokritos.iit.deg.prefers.algebra.PrioritizedPref;
import gr.demokritos.iit.deg.prefers.algebra.SimplePref;

import java.util.List;

/**
 * Created by antonis on 4/4/2017.
 */
public class ConditionRewriter {

    public static ValueExpr getRewritedCondition(Winnow expr) {
        if (expr.getPreference() instanceof SimplePref) {
            return ((SimplePref) expr.getPreference()).getArg();
        }
        else {
            return rewriteInternal(expr.getPreference(), expr.getBase(), expr.getPrefered(), expr.getNonPrefered());
        }
    }

    private static ValueExpr rewriteInternal(ValueExpr expr, List<String> base, List<String> prefered, List<String> nonprefered) {
        if (expr instanceof SimplePref) {
            return ((SimplePref) expr).getArg();
        }
        if (expr instanceof PrioritizedPref) {
            ValueExpr p = rewriteInternal(((PrioritizedPref) expr).getLeftArg(),  base, prefered, nonprefered);
            ValueExpr q = rewriteInternal(((PrioritizedPref) expr).getRightArg(), base, prefered, nonprefered);
            ValueExpr ps = renameConditionSwapVars(p, prefered, nonprefered);
            ValueExpr indiff = new And(new Not(p), new Not(ps));
            return (new Or(p, new And(indiff, q)));
        }
        if (expr instanceof MultidimensionalPref) {
            ValueExpr p = rewriteInternal(((MultidimensionalPref) expr).getLeftArg(),  base, prefered, nonprefered);
            ValueExpr q = rewriteInternal(((MultidimensionalPref) expr).getRightArg(), base, prefered, nonprefered);
            ValueExpr ps = renameConditionSwapVars(p, prefered, nonprefered);
            ValueExpr qs = renameConditionSwapVars(q, prefered, nonprefered);
            return (new Or(new And(p, new Not(qs)),
                           new And(q, new Not(ps))));
        }
        else {
            throw new RuntimeException();
        }
    }

    private static ValueExpr renameConditionSwapVars(ValueExpr expr, List<String> prefered, List<String> nonprefered) {
        ValueExpr newExpr = expr.clone();
        Utils.swapVars(newExpr, prefered, nonprefered);
        return newExpr;
    }
}
