package gr.demokritos.iit.deg.prefers.experiments;

import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by antonis on 6/4/2017.
 */
public class HttpExperiment {

    private static final Logger logger = LoggerFactory.getLogger(HttpExperiment.class);

    private int queryResults;
    private long executionTime;

    public int getLatestQueryResults() {
        return queryResults;
    }

    public long getLatestQueryExecutionTime() {
        return executionTime;
    }

    public void runQuery(String endpointUrl, String queryString) {

        Repository repository = new SPARQLRepository(endpointUrl);
        repository.initialize();

        RepositoryConnection conn = repository.getConnection();

        logger.info("Executing query");

        long start = System.currentTimeMillis();

        TupleQuery query = conn.prepareTupleQuery(queryString);
        TupleQueryResult result = query.evaluate();

        queryResults=0;
        while (result.hasNext()) {
            logger.info(result.next().toString());
            queryResults++;
        }
        long end = System.currentTimeMillis();
        executionTime = end - start;

        logger.info("Found " + queryResults + " results");
        logger.info("Query Execution Time: " + executionTime + " ms");
    }
}
