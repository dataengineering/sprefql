package gr.demokritos.iit.deg.prefers.algebra;

import gr.demokritos.iit.deg.prefers.evaluation.Site;
import org.eclipse.rdf4j.query.algebra.QueryModelVisitor;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.UnaryTupleOperator;

/**
 * Created by antonis on 20/4/2017.
 */

public class RemoteQuery extends UnaryTupleOperator {

    private Site site;

    public RemoteQuery(TupleExpr expr, Site fromSite) {
        super(expr);
        site = fromSite;
    }

    public Site getSite() { return site; }

    public <X extends Exception> void visit(QueryModelVisitor<X> xQueryModelVisitor) throws X {
        xQueryModelVisitor.meetOther(this);
    }

    public String getSignature() {
        StringBuilder sb = new StringBuilder(128);

        sb.append(super.getSignature());
        sb.append(" (source = ");
        sb.append(site.getEndpointUrl());
        sb.append(")");

        return sb.toString();
    }

    @Override
    public int hashCode() {

        return this.site.hashCode() + super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof RemoteQuery) {
            return this.site.equals(((RemoteQuery) o).site) &&
                    super.equals(o);
        }
        return false;
    }
}
