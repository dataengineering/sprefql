package gr.demokritos.iit.deg.prefers.evaluation;

import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.EvaluationStrategy;

/**
 * Created by antonis on 3/4/2017.
 */
public interface WinnowEvaluator {

    void setPreferenceRelationEvaluator(PreferenceRelationEvaluator preferenceRelationEvaluator);

    void setEvaluationStrategy(EvaluationStrategy evaluationStrategy);

    CloseableIteration<BindingSet,QueryEvaluationException> evaluate(Winnow expr, BindingSet bindings);

    boolean isApplicable(TupleExpr tupleExpr);
}
