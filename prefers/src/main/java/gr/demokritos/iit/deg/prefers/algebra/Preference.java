package gr.demokritos.iit.deg.prefers.algebra;

import org.eclipse.rdf4j.query.algebra.ValueExpr;

/**
 * Created by antonis on 21/3/2017.
 */
public interface Preference extends ValueExpr {
}
