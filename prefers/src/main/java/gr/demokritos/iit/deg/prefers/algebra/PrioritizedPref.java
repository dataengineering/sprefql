package gr.demokritos.iit.deg.prefers.algebra;

import org.eclipse.rdf4j.query.algebra.BinaryValueOperator;
import org.eclipse.rdf4j.query.algebra.QueryModelVisitor;

/**
 * Created by antonis on 20/3/2017.
 */
public class PrioritizedPref extends BinaryValueOperator implements Preference {

    public PrioritizedPref(Preference leftArg, Preference rightArg) {
        setLeftArg(leftArg);
        setRightArg(rightArg);
    }

    @Override
    public <X extends Exception> void visit(QueryModelVisitor<X> queryModelVisitor) throws X {
        queryModelVisitor.meetOther(this);
    }


}
