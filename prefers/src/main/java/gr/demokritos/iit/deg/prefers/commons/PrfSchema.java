package gr.demokritos.iit.deg.prefers.commons;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

/**
 * Created by antonis on 7/4/2017.
 */
public final class PrfSchema {

    public static final String NAMESPACE = "http://prefers.deg.iit.demokritos.gr/";

    public static final IRI WINNOW_TYPE;
    public static final IRI WINDOW_SIZE;
    public static final IRI SITE_TYPE;
    public static final IRI ENDPOINT_URL;
    public static final IRI FALLBACK_TO_REWRITE;

    static  {
        ValueFactory vf = SimpleValueFactory.getInstance();
        WINNOW_TYPE = vf.createIRI(NAMESPACE, "winnowType");
        WINDOW_SIZE = vf.createIRI(NAMESPACE, "windowSize");
        SITE_TYPE = vf.createIRI(NAMESPACE, "siteType");
        ENDPOINT_URL = vf.createIRI(NAMESPACE, "endpointUrl");
        FALLBACK_TO_REWRITE = vf.createIRI(NAMESPACE, "fallbackToRewrite");
    }
}

