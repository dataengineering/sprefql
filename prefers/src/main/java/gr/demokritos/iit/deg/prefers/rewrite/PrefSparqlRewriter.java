package gr.demokritos.iit.deg.prefers.rewrite;

import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import org.eclipse.rdf4j.query.algebra.Projection;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.UnaryTupleOperator;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;
import org.eclipse.rdf4j.query.parser.ParsedTupleQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonis on 6/4/2017.
 */
public class PrefSparqlRewriter {

    private WinnowRewriter rewriter = new WinnowRewriter();
    private PrefSPARQLParser parser = new PrefSPARQLParser();
    private SPARQLQueryRenderer sparqlQueryRenderer = new SPARQLQueryRenderer();
    private SPARQLnotExistsQueryRenderer notExistsQueryRenderer = new SPARQLnotExistsQueryRenderer();

    public String rewrite(TupleExpr tupleExpr) throws Exception {
        tupleExpr = substituteWinnow(tupleExpr);
        if (countProjections(tupleExpr) > 1) {
            String rewritted = notExistsQueryRenderer.render(new ParsedTupleQuery(tupleExpr));
            return removeStringDatatypes(rewritted);
        }
        else {
            String rewritted = sparqlQueryRenderer.render(new ParsedTupleQuery(tupleExpr));
            return removeStringDatatypes(rewritted);
        }
    }

    public String rewrite(String query) throws Exception {
        TupleExpr tupleExpr = parser.parseQuery(query,null).getTupleExpr();
        return rewrite(tupleExpr);
    }

    public void setSuffix(String suffix) {
        rewriter.setSuffix(suffix);
    }

    private String removeStringDatatypes(String queryStr) {
        return queryStr.replaceAll("\\^\\^<http://www.w3.org/2001/XMLSchema#string>","");
    }

    private TupleExpr substituteWinnow(TupleExpr input) {
        if (input instanceof Winnow) {
            return rewriter.rewrite((Winnow) input);
        }
        else {
            TupleExpr tupleExpr = input.clone();
            try {
                tupleExpr.visit(new AbstractQueryModelVisitor<Exception>() {
                    @Override
                    protected void meetUnaryTupleOperator(UnaryTupleOperator node) throws Exception {
                        if (node instanceof Winnow) {
                            node.replaceWith(rewriter.rewrite((Winnow) node));
                        } else {
                            super.meetUnaryTupleOperator(node);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            return tupleExpr;
        }
    }

    private int countProjections(TupleExpr expr) {
        List set = new ArrayList();
        expr.visit(new AbstractQueryModelVisitor<RuntimeException>() {
            @Override
            public void meet(Projection node) throws RuntimeException {
                super.meet(node);
                set.add(true);
            }
        });
        return set.size();
    }
}
