package gr.demokritos.iit.deg.prefers.evaluation;

import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.common.iteration.EmptyIteration;
import org.eclipse.rdf4j.common.iteration.LookAheadIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.resultio.TupleQueryResultParser;
import org.eclipse.rdf4j.query.resultio.TupleQueryResultWriter;
import org.eclipse.rdf4j.query.resultio.helpers.QueryResultCollector;
import org.eclipse.rdf4j.query.resultio.sparqljson.SPARQLResultsJSONParser;
import org.eclipse.rdf4j.query.resultio.sparqljson.SPARQLResultsJSONWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by antonis on 31/3/2017.
 */
public class BNLtempFile {

    private String prefix = "/tmp/BNLtmpfile-";
    private String suffix = ".json";

    private File file;
    private TupleQueryResultWriter writer = null;
    private TupleQueryResultParser reader = null;

    public BNLtempFile() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyy-hhmmss.SSS");
        file = new File(prefix + simpleDateFormat.format( new Date() ) + suffix);
    }

    public boolean isEmpty() {
        return (file.length() == 0);
    }

    public void fillWith(CloseableIteration<BindingSet, QueryEvaluationException> iteration) {
        boolean first = true;
        if (writer == null) {
            try {
                writer = new SPARQLResultsJSONWriter(new FileOutputStream(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                while (iteration.hasNext()) {
                    BindingSet bindingSet = iteration.next();
                    if (first) {
                        writer.startQueryResult(new ArrayList<>(bindingSet.getBindingNames()));
                        first = false;
                    }
                    writer.handleSolution(bindingSet);
                }
            }
            finally {
                iteration.close();
            }
            if (!first) {
                writer.endQueryResult();
            }
        }
    }

    public CloseableIteration<BindingSet, QueryEvaluationException> scan() {
        if (file.length() == 0) {
            return new EmptyIteration<>();
        }
        reader = new SPARQLResultsJSONParser();
        QueryResultCollector handler = new QueryResultCollector();
        reader.setQueryResultHandler(handler);

        try {
            reader.parseQueryResult(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Iterator<BindingSet> iterator = handler.getBindingSets().iterator();

        return (new LookAheadIteration<BindingSet, QueryEvaluationException>() {
            @Override
            protected BindingSet getNextElement() throws QueryEvaluationException {
                while (iterator.hasNext()) {
                    return iterator.next();
                }
                return null;
            }
        });
    }

}
