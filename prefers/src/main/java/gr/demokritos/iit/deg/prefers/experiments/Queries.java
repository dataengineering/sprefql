package gr.demokritos.iit.deg.prefers.experiments;

/**
 * Created by antonis on 5/5/2017.
 */
public final class Queries {

    public final static String prefixes = ""
            + "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n"
            + "PREFIX dc: <http://purl.org/dc/terms/>\n"
            ;

    // Query bodies

    public final static String title_genre_all = prefixes
            + "SELECT ?title ?genre \n"
            + " (xsd:float(?runtimeStr) \n"
            + "   as ?runtime) \n"
            + "WHERE {\n"
            + " ?s a movie:film.\n"
            + " ?s dc:title ?title.\n"
            + " ?s movie:genre ?g.\n"
            + " ?g movie:film_genre_name ?genre.\n"
            + " ?s movie:runtime ?runtimeStr.\n"
            + "}\n"
            ;

    public final static String title_runtime_year_action = prefixes
            + "SELECT ?title \n"
            + " (xsd:float(?runtimeStr) \n"
            + "   as ?runtime)\n"
            + " (xsd:int(substr(?dateStr,1,4)) \n"
            + "   as ?year) \n"
            + "WHERE {\n"
            + " ?a a movie:film.\n"
            + " ?a movie:genre ?g.\n"
            + " ?g movie:film_genre_name \"Action\".\n"
            + " ?a movie:runtime ?runtimeStr.\n"
            + " ?a dc:title ?title.\n"
            + " ?a dc:date ?dateStr.\n"
            + "}\n"
            ;

    public final static String film_title_madmax = prefixes
            + "SELECT ?film ?title\n"
            + "WHERE {\n"
            + " ?film a movie:film .\n"
            + " ?film dc:title ?title.\n"
            + " FILTER regex(?title, \"Mad Max\").\n"
            + "}\n"
            ;

    public final static String title_runtime_year_all = prefixes
            + "SELECT ?title \n"
            + " (xsd:float(?runtimeStr) \n"
            + "   as ?runtime)\n"
            + " (xsd:int(substr(?dateStr,1,4)) \n"
            + "   as ?year) \n"
            + "WHERE {\n"
            + " ?a a movie:film.\n"
            + " ?a movie:runtime ?runtimeStr.\n"
            + " ?a dc:title ?title.\n"
            + " ?a dc:date ?dateStr.\n"
            + "}\n"
            ;

    public final static String film_title_all = prefixes
            + "SELECT ?film ?title\n"
            + "WHERE {\n"
            + " ?film a movie:film .\n"
            + " ?film dc:title ?title.\n"
            + " ?film movie:performance ?performance.\n"
            + " ?performance movie:performance_character ?character.\n"
            + " FILTER (?character = \"James Bond\").\n"
            + "}"
            ;

    // Preferences

    public final static String long_per_genre = ""
            + "PREFER (?title1 ?genre1 ?runtime1) \n"
            + "TO     (?title2 ?genre2 ?runtime2) \n"
            + "IF (?genre1 = ?genre2 && \n"
            + "    ?runtime1 > ?runtime2) \n"
            ;

    public final static String long_recent_equal = ""
            + "PREFER (?title1 ?runtime1 ?year1) \n"
            + "TO     (?title2 ?runtime2 ?year2) \n"
            + "IF (?runtime1 > ?runtime2) \n"
            + "   AND (?year1 > ?year2) \n"
            ;

    public final static String between_prior_to_recent = ""
            + "PREFER (?title1 ?run1 ?year1) \n"
            + "TO     (?title2 ?run2 ?year2)\n"
            + "IF (?run1 >= 115 && ?run1 <= 125 \n"
            + "   && (?run2 < 115 || ?run2 > 125)) \n"
            + "   PRIOR TO (?year1 >= 2005 \n"
            + "    && ?year2 < 2005) \n"
            ;

    public final static String sequel = ""
            + "PREFER (?film1 ?title1)\n"
            + "TO     (?film2 ?title2)\n"
            + "IF EXISTS { \n"
            + "   ?film1 movie:sequel ?film2 \n"
            + "} \n"
            ;

    // Queries

    public final static String Q1 = title_genre_all + long_per_genre;
    public final static String Q2 = title_runtime_year_action + long_recent_equal;
    public final static String Q3 = title_runtime_year_action + between_prior_to_recent;
    public final static String Q4 = film_title_madmax + sequel;
    public final static String Q5 = title_runtime_year_all + long_recent_equal;
    public final static String Q6 = title_runtime_year_all + between_prior_to_recent;
    public final static String Q7 = film_title_all + sequel;

    public final static String B1 = title_genre_all;
    public final static String B2 = title_runtime_year_action;
    public final static String B4 = film_title_madmax;
    public final static String B5 = title_runtime_year_all;
    public final static String B7 = film_title_all;
}
