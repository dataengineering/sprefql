package gr.demokritos.iit.deg.prefers.repository;

import gr.demokritos.iit.deg.prefers.sail.PrefSailImplConfig;
import org.apache.http.client.HttpClient;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.config.RepositoryConfigException;
import org.eclipse.rdf4j.repository.config.RepositoryFactory;
import org.eclipse.rdf4j.repository.config.RepositoryRegistry;
import org.eclipse.rdf4j.repository.manager.RepositoryInfo;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.manager.SystemRepository;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by antonis on 29/6/2017.
 */
public class PrefRepositoryManager extends RepositoryManager {

    private static final Logger logger = LoggerFactory.getLogger(PrefRepositoryManager.class);

    String configFile = "/etc/prefers/repository.ttl";

    @Override
    protected Repository createRepository(String s) throws RepositoryConfigException, RepositoryException {

        Model configModel = null;
        try {
            logger.info("Parsing config file {}", configFile);
            configModel = Rio.parse(new FileInputStream(new File(configFile)), "", RDFFormat.TURTLE);
        } catch (IOException e) {
            throw new RepositoryConfigException("File not found!");
        }

        SailRepositoryConfig config = new SailRepositoryConfig();

        MemoryStoreConfig memoryStoreConfig = new MemoryStoreConfig();
        PrefSailImplConfig prefSailConfig = new PrefSailImplConfig(memoryStoreConfig);
        prefSailConfig.parse(configModel, null);
        config.setSailImplConfig(prefSailConfig);

        Optional<RepositoryFactory> factory = RepositoryRegistry.getInstance().get(config.getType());

        if (!factory.isPresent()) {
            throw new RepositoryConfigException("Factory not present!");
        }

        Repository repository = factory.get().getRepository(config);

        logger.info("Created new repository manager");

        repository.initialize();

        return repository;
    }

    @Override
    protected Repository createSystemRepository() throws RepositoryException {
        try {
            return new SystemRepository(File.createTempFile("pref-system", ""));
        } catch (IOException e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public RepositoryInfo getRepositoryInfo(String s) throws RepositoryException {
        return null;
    }

    @Override
    public Collection<RepositoryInfo> getAllRepositoryInfos(boolean b) throws RepositoryException {
        return null;
    }

    @Override
    protected void cleanUpRepository(String s) throws IOException {

    }

    @Override
    public URL getLocation() throws MalformedURLException {
        return null;
    }

    @Override
    public HttpClient getHttpClient() {
        return null;
    }

    @Override
    public void setHttpClient(HttpClient httpClient) {

    }
}
