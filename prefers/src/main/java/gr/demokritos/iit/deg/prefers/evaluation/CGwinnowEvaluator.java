package gr.demokritos.iit.deg.prefers.evaluation;

import csapps.layout.algorithms.hierarchicalLayout.Edge;
import csapps.layout.algorithms.hierarchicalLayout.Graph;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.EvaluationStrategy;
import org.eclipse.rdf4j.query.impl.IteratingTupleQueryResult;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CGwinnowEvaluator implements WinnowEvaluator
{
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger( CGwinnowEvaluator.class );

    private PreferenceRelationEvaluator preferenceRelationEvaluator;
    private EvaluationStrategy evaluationStrategy;

    public void setPreferenceRelationEvaluator(PreferenceRelationEvaluator preferenceRelationEvaluator) {
        this.preferenceRelationEvaluator = preferenceRelationEvaluator;
    }

    public void setEvaluationStrategy(EvaluationStrategy evaluationStrategy) {
        this.evaluationStrategy = evaluationStrategy;
    }

    public CloseableIteration<BindingSet,QueryEvaluationException> evaluate( Winnow expr, BindingSet bindings )
    {
        BindingSetMapper mapper = new BindingSetMapper( expr );
        preferenceRelationEvaluator.setMapper( mapper );
        List<BindingSet> bindingSetList = new ArrayList<>();

        int edge_count = 0;
        ArrayList<BindingSet> nodes = new ArrayList<BindingSet>();
        ArrayList<Edge> edges = new ArrayList<Edge>();
        Graph graph;

        CloseableIteration<BindingSet,QueryEvaluationException> iteration = evaluationStrategy.evaluate(expr.getArg(), bindings);
        while (iteration.hasNext()) {
            bindingSetList.add(iteration.next());
        }

        Iterator<BindingSet> outerIter = bindingSetList.iterator();

        while( outerIter.hasNext() ) {
        	BindingSet b1 = outerIter.next();
            Iterator<BindingSet> innerIter = bindingSetList.iterator();
        	while (innerIter.hasNext()) {
            	BindingSet b2 = innerIter.next();
        		
        		int b1_index = nodes.indexOf( b1 );
        		if( b1_index == -1 ) {
        			nodes.add( b1 );
        			b1_index = nodes.indexOf( b1 );
        			logger.info( "Adding " + b1 + " at " + b1_index );
        		}
        		int b2_index = nodes.indexOf( b2 );
        		if( b2_index == -1 ) {
        			nodes.add( b2 );
        			b2_index = nodes.indexOf( b2 );
        			logger.info( "Adding " + b2 + " at " + b2_index );
        		}

        		if( preferenceRelationEvaluator.isPreferred(expr.getPreference(), b2, b1) ) {
        			edges.add( new Edge(b1_index, b2_index) );
        			++edge_count;
                }
            }
        }
        Edge[] e = new Edge[ edge_count ];
        edges.toArray( e );
        graph = new Graph( nodes.size(), e );
        logger.info( graph.toString() );
        for( BindingSet b : nodes ) { logger.info( b.toString() ); }

        int[] layers = graph.getReducedGraph().getVertexLayers();
        
        ArrayList<BindingSet> bb = new ArrayList<BindingSet>();
        for( int i=0; i < layers.length; ++i ) {
        	if( layers[i] == 1 ) { bb.add( nodes.get(i)); }
        }
        
        List<String> bindingNames = new ArrayList<String>( bindings.size() );
        bindingNames.addAll( bindings.getBindingNames() ); 
        IteratingTupleQueryResult retv = new IteratingTupleQueryResult( bindingNames, bb );
        return retv;
    }

    @Override
    public boolean isApplicable(TupleExpr tupleExpr) {
        return true;
    }

}
