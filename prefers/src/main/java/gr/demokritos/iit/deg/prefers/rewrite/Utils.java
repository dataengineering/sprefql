package gr.demokritos.iit.deg.prefers.rewrite;

import org.eclipse.rdf4j.query.algebra.ValueExpr;
import org.eclipse.rdf4j.query.algebra.Var;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;

import java.util.List;

/**
 * Created by antonis on 4/4/2017.
 */
public class Utils {

    public static void renameVars(ValueExpr expr, List<String> from, List<String> to) {
        try {
            expr.visit(new AbstractQueryModelVisitor<Exception>() {
                @Override
                public void meet(Var node) {
                    if (from.contains(node.getName())) {
                        int i = from.indexOf(node.getName());
                        node.setName(to.get(i));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void swapVars(ValueExpr expr, List<String> u, List<String> v) {
        try {
            expr.visit(new AbstractQueryModelVisitor<Exception>() {
                @Override
                public void meet(Var node) {
                    if (u.contains(node.getName())) {
                        int i = u.indexOf(node.getName());
                        node.setName(v.get(i));
                    }
                    else {
                        if (v.contains(node.getName())) {
                            int i = v.indexOf(node.getName());
                            node.setName(u.get(i));
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
