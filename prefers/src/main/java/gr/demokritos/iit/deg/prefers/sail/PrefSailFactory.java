package gr.demokritos.iit.deg.prefers.sail;

import gr.demokritos.iit.deg.prefers.evaluation.*;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.config.SailConfigException;
import org.eclipse.rdf4j.sail.config.SailFactory;
import org.eclipse.rdf4j.sail.config.SailImplConfig;

/**
 * Created by angel on 6/4/2017.
 */
public class PrefSailFactory implements SailFactory {

    public static final String SAIL_TYPE = "prefsail";

    public String getSailType() { return SAIL_TYPE; }

    public SailImplConfig getConfig() {
        return new PrefSailImplConfig();
    }

    public Sail getSail(SailImplConfig config) {

        if (!SAIL_TYPE.equals(config.getType())) {
            throw new SailConfigException("Invalid Sail type: " + config.getType());
        }

        PrefSail sail = new PrefSail();

        PrefSailImplConfig prefSailConfig = (PrefSailImplConfig) config;
        WinnowEvaluator winnowEvaluator;
        Site site;

        switch (prefSailConfig.getWinnowType()) {
            case NL:
                winnowEvaluator = new NLwinnowEvaluator();
                break;
            case BNL:
                winnowEvaluator = new BNLwinnowEvaluator();
                break;
            case CG:
                winnowEvaluator = new CGwinnowEvaluator();
                break;
            case RW:
                winnowEvaluator = new RWwinnowEvaluator();
                break;
            default:
                winnowEvaluator = new BNLwinnowEvaluator();
                break;
        }

        if (winnowEvaluator instanceof BNLwinnowEvaluator) {
            ((BNLwinnowEvaluator)winnowEvaluator).setWindowSize(prefSailConfig.getWindowSize());
        }

        switch (prefSailConfig.getSiteType()) {
            case LOCAL:
                site = new Site();
                break;
            case REMOTE:
                site = new Site(prefSailConfig.getEndpointUrl());
                break;
            default:
                site = new Site();
                break;
        }

        sail.setWinnowEvaluator(winnowEvaluator);
        sail.setSite(site);
        sail.setFallbackToRewrite(prefSailConfig.fallbackToRewrite());

        return sail;
    }


}
