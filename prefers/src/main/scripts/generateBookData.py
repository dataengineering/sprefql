import random
import sys

def pretty_string(prefix, num, width):
  return prefix + str(num).zfill(width)
  
def make_list(prefix, size):
  width = len(str(size))
  return map(lambda num: pretty_string(prefix,num,width), range(1,size+1))

def print_uri(string):
  return "<http://example.org/" + string + ">"

def print_string(string):
  return "\"" + string + "\""

def print_price(p,q):
  return str(p) + "." + str(q)

if len(sys.argv) < 4:
  print "Usage: python generateTupleData.py [#records] [#books] [#vendors]"
  exit()

records = int(sys.argv[1])
books = int(sys.argv[2])
vendors = int(sys.argv[3])

isbn = make_list("i",books)
vendor = make_list("v",vendors)

pricefrom = 8
priceto = 55

for x in range(1,records+1):
  i = random.choice(isbn);
  v = random.choice(vendor)
  p = random.randrange(pricefrom,priceto)
  
  print print_uri("b" + str(x)) + " " + print_uri("isbn")   + " " + print_string(i) + " ."
  print print_uri("b" + str(x)) + " " + print_uri("vendor") + " " + print_string(v) + " ."
  print print_uri("b" + str(x)) + " " + print_uri("price")  + " " + print_price(p,(x+records)) + " ."
  
