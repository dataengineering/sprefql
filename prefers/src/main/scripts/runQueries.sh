#!/usr/bin/env bash

scriptdir=`dirname "$BASH_SOURCE"`

mainclass=gr.demokritos.iit.deg.prefers.experiments.LinkedMDBExperimentReloaded

targetdir=$scriptdir"/../../../target"

for jar in `ls $targetdir/*.jar  $targetdir/dependency/*.jar`
do classpath=$classpath:$jar; done

java -Xmx1560m -cp $classpath $mainclass $*
