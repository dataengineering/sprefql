package gr.demokritos.iit.deg.prefers;

import gr.demokritos.iit.deg.prefers.algebra.RemoteQuery;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import gr.demokritos.iit.deg.prefers.evaluation.*;
import gr.demokritos.iit.deg.prefers.experiments.Queries;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import gr.demokritos.iit.deg.prefers.rewrite.PrefSparqlRewriter;
import junit.framework.TestCase;
import org.eclipse.rdf4j.query.algebra.*;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.junit.Test;

/**
 * Created by antonis on 3/7/2017.
 */
public class PrefSPARQLParserTest extends TestCase {

    private PrefSPARQLParser parser = new PrefSPARQLParser();
    private PrefSparqlRewriter rewriter = new PrefSparqlRewriter();

    private String query1 = "" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>" +
            "PREFIX dc: <http://purl.org/dc/terms/>\n" +
            "SELECT ?title ?genre \n" +
            " (xsd:float(?runtimeStr) \n" +
            "   as ?runtime) \n" +
            "WHERE {\n" +
            " ?s a movie:film.\n" +
            " ?s dc:title ?title.\n" +
            " ?s movie:genre ?g.\n" +
            " ?g movie:film_genre_name ?genre.\n" +
            " ?s movie:runtime ?runtimeStr.\n" +
            "}\n" +
            "PREFER (?title1 ?genre1 ?runtime1) \n" +
            "TO     (?title2 ?genre2 ?runtime2) \n" +
            "IF (?genre1 = ?genre2 && ?runtime1 > ?runtime2) \n" +
            "ORDER BY ?runtime \n" +
            "LIMIT 10 \n" +
            "OFFSET 10";

    private String query2 = "" +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
            "PREFIX lingvo: <http://www.lingvoj.org/lingvo/>\n" +
            "SELECT ?director ?movie (count(?l) as ?languages)\n" +
            "WHERE {\n" +
            "  ?d a movie:director .\n" +
            "  ?d rdfs:label ?director .\n" +
            "  ?f movie:director ?d .\n" +
            "  ?f movie:language lingvo:el .\n" +
            "  ?f movie:language ?l .\n" +
            "  ?f rdfs:label ?movie .\n" +
            "  ?f movie:runtime ?runtume\n" +
            "} \n" +
            "GROUP BY ?director ?movie\n" +
            "PREFER (?d1 ?m1 ?l1) TO (?d2 ?m2 ?l2)\n" +
            "IF (?l1 = 1 && ?l2 > 1)\n" +
            "LIMIT 1";

    @Test
    public void testQuery1()
            throws Exception
    {
        ParsedQuery q = parser.parseQuery(query1, null);

        assertNotNull(q);
        TupleExpr result = q.getTupleExpr();

        assertTrue(result instanceof Slice);
        result = ((Slice) result).getArg();
        assertTrue(result instanceof Order);
        result = ((Order) result).getArg();
        assertTrue(result instanceof Winnow);
        result = ((Winnow) result).getArg();
        assertTrue(result instanceof Projection);
        result = ((Projection) result).getArg();
        assertTrue(result instanceof Extension);
        result = ((Extension) result).getArg();
        assertTrue(result instanceof Join);
    }

    @Test
    public void testQuery2()
            throws Exception
    {
        ParsedQuery q = parser.parseQuery(query2, null);

        assertNotNull(q);
        TupleExpr result = q.getTupleExpr();

        assertTrue(result instanceof Slice);
        result = ((Slice) result).getArg();
        assertTrue(result instanceof Winnow);
        result = ((Winnow) result).getArg();
        assertTrue(result instanceof Projection);
        result = ((Projection) result).getArg();
        assertTrue(result instanceof Extension);
        result = ((Extension) result).getArg();
        assertTrue(result instanceof Group);
        result = ((Group) result).getArg();
        assertTrue(result instanceof Join);
    }

    @Test
    public void testQuery3()
            throws Exception
    {
        String result = "select ?title ?genre (<http://www.w3.org/2001/XMLSchema#float>(?runtimeStr) as ?runtime)\n" +
                "where {\n" +
                "  ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
                "  ?s <http://purl.org/dc/terms/title> ?title.\n" +
                "  ?s <http://data.linkedmdb.org/resource/movie/genre> ?g.\n" +
                "  ?g <http://data.linkedmdb.org/resource/movie/film_genre_name> ?genre.\n" +
                "  ?s <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
                "  filter ( !( exists {  ?s_tmp <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
                "  ?s_tmp <http://purl.org/dc/terms/title> ?title1.\n" +
                "  ?s_tmp <http://data.linkedmdb.org/resource/movie/genre> ?g_tmp.\n" +
                "  ?g_tmp <http://data.linkedmdb.org/resource/movie/film_genre_name> ?genre1.\n" +
                "  ?s_tmp <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_tmp.\n" +
                "  filter  ((?genre1 = ?genre) && (<http://www.w3.org/2001/XMLSchema#float>(?runtimeStr_tmp) > <http://www.w3.org/2001/XMLSchema#float>(?runtimeStr))).\n" +
                "})).\n" +
                "}\n" +
                "order by ?runtime\n" +
                "limit 10\n" +
                "offset 10";

        ParsedQuery q = parser.parseQuery(query1, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(query1);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery4()
            throws Exception
    {
        String result = "select ?director ?movie ?languages\n" +
                "where {\n" +
                "  {\n" +
                "   select ?director ?movie ( count(?l) as ?languages)\n" +
                "   where {\n" +
                "     ?d <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/director>.\n" +
                "     ?d <http://www.w3.org/2000/01/rdf-schema#label> ?director.\n" +
                "     ?f <http://data.linkedmdb.org/resource/movie/director> ?d.\n" +
                "     ?f <http://data.linkedmdb.org/resource/movie/language> <http://www.lingvoj.org/lingvo/el>.\n" +
                "     ?f <http://data.linkedmdb.org/resource/movie/language> ?l.\n" +
                "     ?f <http://www.w3.org/2000/01/rdf-schema#label> ?movie.\n" +
                "     ?f <http://data.linkedmdb.org/resource/movie/runtime> ?runtume.\n" +
                "    }\n" +
                "    group by ?director ?movie\n" +
                "  }\n" +
                "  filter ( !( exists {  {\n" +
                "   select ?d1 ?m1 ( count(?l_tmp) as ?l1)\n" +
                "   where {\n" +
                "     ?d_tmp <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/director>.\n" +
                "     ?d_tmp <http://www.w3.org/2000/01/rdf-schema#label> ?d1.\n" +
                "     ?f_tmp <http://data.linkedmdb.org/resource/movie/director> ?d_tmp.\n" +
                "     ?f_tmp <http://data.linkedmdb.org/resource/movie/language> <http://www.lingvoj.org/lingvo/el>.\n" +
                "     ?f_tmp <http://data.linkedmdb.org/resource/movie/language> ?l_tmp.\n" +
                "     ?f_tmp <http://www.w3.org/2000/01/rdf-schema#label> ?m1.\n" +
                "     ?f_tmp <http://data.linkedmdb.org/resource/movie/runtime> ?runtume_tmp.\n" +
                "   }\n" +
                "   group by ?m1 ?d1\n" +
                " }\n" +
                " filter  ((?l1 = \"\"\"1\"\"\"^^<http://www.w3.org/2001/XMLSchema#integer>) && (?languages > \"\"\"1\"\"\"^^<http://www.w3.org/2001/XMLSchema#integer>)).\n" +
                "})).\n" +
                "}\n" +
                "limit 1";

        ParsedQuery q = parser.parseQuery(query2, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(query2);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery5() {

        TupleExpr expr = parser.parseQuery(query1, null).getTupleExpr();

        RemoteSiteEnforcer enforcer = new RemoteSiteEnforcer();
        enforcer.setSite(new Site("http://dbpedia.org/sparql"));

        TupleExpr result = enforcer.enforceSite(expr);

        assertTrue(result instanceof Slice);
        result = ((Slice) result).getArg();
        assertTrue(result instanceof Order);
        result = ((Order) result).getArg();
        assertTrue(result instanceof Winnow);
        result = ((Winnow) result).getArg();
        assertTrue(result instanceof RemoteQuery);
    }

    @Test
    public void testQuery6() {

        TupleExpr expr = parser.parseQuery(query2, null).getTupleExpr();

        RemoteSiteEnforcer enforcer = new RemoteSiteEnforcer();
        enforcer.setSite(new Site("http://dbpedia.org/sparql"));

        TupleExpr result = enforcer.enforceSite(expr);

        assertTrue(result instanceof Slice);
        result = ((Slice) result).getArg();
        assertTrue(result instanceof Winnow);
        result = ((Winnow) result).getArg();
        assertTrue(result instanceof RemoteQuery);
    }

    @Test
    public void testApplicability() {

        WinnowEvaluator BNL = new BNLwinnowEvaluator();
        WinnowEvaluator NL = new NLwinnowEvaluator();
        WinnowEvaluator CG = new CGwinnowEvaluator();
        WinnowEvaluator RW = new RWwinnowEvaluator();

        TupleExpr q1 = parser.parseQuery(Queries.Q1, null).getTupleExpr();
        TupleExpr q2 = parser.parseQuery(Queries.Q2, null).getTupleExpr();
        TupleExpr q3 = parser.parseQuery(Queries.Q3, null).getTupleExpr();
        TupleExpr q4 = parser.parseQuery(Queries.Q4, null).getTupleExpr();
        TupleExpr q5 = parser.parseQuery(Queries.Q5, null).getTupleExpr();
        TupleExpr q6 = parser.parseQuery(Queries.Q6, null).getTupleExpr();
        TupleExpr q7 = parser.parseQuery(Queries.Q7, null).getTupleExpr();

        assertTrue(NL.isApplicable(q1));
        assertTrue(NL.isApplicable(q2));
        assertTrue(NL.isApplicable(q3));
        assertTrue(NL.isApplicable(q4));
        assertTrue(NL.isApplicable(q5));
        assertTrue(NL.isApplicable(q6));
        assertTrue(NL.isApplicable(q7));

        assertTrue(BNL.isApplicable(q1));
        assertFalse(BNL.isApplicable(q2));
        assertFalse(BNL.isApplicable(q3));
        assertFalse(BNL.isApplicable(q4));
        assertFalse(BNL.isApplicable(q5));
        assertFalse(BNL.isApplicable(q6));
        assertFalse(BNL.isApplicable(q7));

        assertTrue(CG.isApplicable(q1));
        assertTrue(CG.isApplicable(q2));
        assertTrue(CG.isApplicable(q3));
        assertTrue(CG.isApplicable(q4));
        assertTrue(CG.isApplicable(q5));
        assertTrue(CG.isApplicable(q6));
        assertTrue(CG.isApplicable(q7));

        assertFalse(RW.isApplicable(q1));
        assertFalse(RW.isApplicable(q2));
        assertFalse(RW.isApplicable(q3));
        assertFalse(RW.isApplicable(q4));
        assertFalse(RW.isApplicable(q5));
        assertFalse(RW.isApplicable(q6));
        assertFalse(RW.isApplicable(q7));
    }

}
