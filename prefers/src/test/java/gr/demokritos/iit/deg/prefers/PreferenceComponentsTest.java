package gr.demokritos.iit.deg.prefers;

import gr.demokritos.iit.deg.prefers.algebra.Preference;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import gr.demokritos.iit.deg.prefers.evaluation.*;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import gr.demokritos.iit.deg.prefers.commons.Language;
import gr.demokritos.iit.deg.prefers.rewrite.SPARQLQueryRenderer;
import gr.demokritos.iit.deg.prefers.rewrite.WinnowRewriter;
import gr.demokritos.iit.deg.prefers.sail.PrefSailImplConfig;
import junit.framework.TestCase;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.*;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.evaluation.federation.FederatedServiceResolver;
import org.eclipse.rdf4j.query.algebra.evaluation.federation.FederatedServiceResolverImpl;
import org.eclipse.rdf4j.query.impl.EmptyBindingSet;
import org.eclipse.rdf4j.query.impl.MapBindingSet;
import org.eclipse.rdf4j.query.parser.ParsedTupleQuery;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.config.RepositoryFactory;
import org.eclipse.rdf4j.repository.config.RepositoryRegistry;
import org.eclipse.rdf4j.repository.evaluation.RepositoryTripleSource;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;


/**
 * Created by antonis on 28/3/2017.
 */
public class PreferenceComponentsTest extends TestCase {

    private PrefSPARQLParser parser = new PrefSPARQLParser();
    private ValueFactory vf = SimpleValueFactory.getInstance();

    private String query1 = "" +
            "PREFIX ex: <http://example.org/>" +
            "SELECT ?isbn ?vendor ?price\n" +
            "WHERE {\n" +
            "  ?s ex:isbn ?isbn .\n" +
            "  ?s ex:vendor ?vendor .\n" +
            "  ?s ex:price ?price .\n" +
            "}\n" +
            "PREFER (?i1 ?v1 ?p1) TO (?i2 ?v2 ?p2)\n" +
            "IF (?i1 = ?i2 && ?p1 < ?p2)";

    @Test
    public void testVarNames1() {

        TupleExpr expr = parser.parseQuery(query1,null).getTupleExpr();
        BindingSetMapper mapper = new BindingSetMapper(expr);

        MapBindingSet b1 = new MapBindingSet();
        b1.addBinding("isbn",vf.createLiteral("6969"));
        b1.addBinding("vendor",vf.createLiteral("amazon"));
        b1.addBinding("price",vf.createLiteral(10.5));

        MapBindingSet b2 = new MapBindingSet();
        b2.addBinding("isbn",vf.createLiteral("8888"));
        b2.addBinding("vendor",vf.createLiteral("amazon"));
        b2.addBinding("price",vf.createLiteral(9.9));

        MapBindingSet b3 = new MapBindingSet();
        b2.addBinding("isbn",vf.createLiteral("8888"));
        b2.addBinding("vendor",vf.createLiteral("amazon"));
        b2.addBinding("price",vf.createLiteral(9.9));

        MapBindingSet r = new MapBindingSet();
        r.addBinding("i1",vf.createLiteral("6969"));
        r.addBinding("v1",vf.createLiteral("amazon"));
        r.addBinding("p1",vf.createLiteral(10.5));
        r.addBinding("i2",vf.createLiteral("8888"));
        r.addBinding("v2",vf.createLiteral("amazon"));
        r.addBinding("p2",vf.createLiteral(9.9));

        assertEquals(mapper.get(b1,b2), r);
    }

    @Test
    public void testPreferenceEvaluationStrategy() {

        TupleExpr expr = parser.parseQuery(query1,null).getTupleExpr();
        BindingSetMapper mapper = new BindingSetMapper(expr);
        Preference preference = ((Winnow) expr).getPreference();

        Repository rep = new SailRepository(new MemoryStore());
        rep.initialize();
        RepositoryConnection conn = rep.getConnection();

        PreferenceRelationEvaluator evaluator = new PreferenceRelationEvaluator(new RepositoryTripleSource(conn), new FederatedServiceResolverImpl());
        evaluator.setMapper(mapper);

        MapBindingSet b1 = new MapBindingSet();
        b1.addBinding("isbn",vf.createLiteral("6969"));
        b1.addBinding("vendor",vf.createLiteral("amazon"));
        b1.addBinding("price",vf.createLiteral(10.5));

        MapBindingSet b2 = new MapBindingSet();
        b2.addBinding("isbn",vf.createLiteral("8888"));
        b2.addBinding("vendor",vf.createLiteral("amazon"));
        b2.addBinding("price",vf.createLiteral(9.9));

        MapBindingSet b3 = new MapBindingSet();
        b3.addBinding("isbn",vf.createLiteral("8888"));
        b3.addBinding("vendor",vf.createLiteral("amazon"));
        b3.addBinding("price",vf.createLiteral(15.1));

        assertEquals(evaluator.isPreferred(preference, b1, b2), false);
        assertEquals(evaluator.isPreferred(preference, b3, b2), false);
        assertEquals(evaluator.isPreferred(preference, b2, b3), true);
    }

    @Test
    public void testWinnowNestedLoops() {

        TupleExpr expr = parser.parseQuery(query1,null).getTupleExpr();


        SailRepositoryConfig config = new SailRepositoryConfig();
        MemoryStoreConfig memoryStoreConfig = new MemoryStoreConfig();
        config.setSailImplConfig(memoryStoreConfig);
        Optional<RepositoryFactory> factory = RepositoryRegistry.getInstance().get(config.getType());

        if (!factory.isPresent()) {
            fail();
        }

        Repository rep = factory.get().getRepository(config);

        //Repository rep = new SailRepository(new MemoryStore());

        rep.initialize();

        RepositoryConnection conn = rep.getConnection();

        ValueFactory vf = rep.getValueFactory();


        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/price"), vf.createLiteral(14.75));

        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/price"), vf.createLiteral(13.50));

        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("qualityBooks"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/price"), vf.createLiteral(18.80));

        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0062059041"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/price"), vf.createLiteral(7.30));

        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0374164770"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/price"), vf.createLiteral(21.88));


        RepositoryTripleSource source = new RepositoryTripleSource(conn);
        FederatedServiceResolver resolver = new FederatedServiceResolverImpl();

        PreferentialEvaluationStrategy strategy = new PreferentialEvaluationStrategy(source, resolver);
        PreferenceRelationEvaluator preferenceRelationEvaluator = new PreferenceRelationEvaluator(source, resolver);
        WinnowEvaluator evaluator = new NLwinnowEvaluator();

        evaluator.setPreferenceRelationEvaluator(preferenceRelationEvaluator);
        evaluator.setEvaluationStrategy(strategy);
        strategy.setWinnowEvaluator(evaluator);

        CloseableIteration<BindingSet,QueryEvaluationException> iteration = strategy.evaluate(expr, new EmptyBindingSet());

        int count=0;
        while (iteration.hasNext()) {
            System.out.println("NL: " + iteration.next());
            count++;
        }
        System.out.println("");

        assertEquals(3, count);
    }

    @Test
    public void testWinnowBlockNestedLoops() {

        TupleExpr expr = parser.parseQuery(query1,null).getTupleExpr();

        Repository rep = new SailRepository(new MemoryStore());
        rep.initialize();

        RepositoryConnection conn = rep.getConnection();

        ValueFactory vf = rep.getValueFactory();


        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/price"), vf.createLiteral(14.75));

        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/price"), vf.createLiteral(13.50));

        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("qualityBooks"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/price"), vf.createLiteral(18.80));

        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0062059041"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/price"), vf.createLiteral(7.30));

        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0374164770"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/price"), vf.createLiteral(21.88));


        RepositoryTripleSource source = new RepositoryTripleSource(conn);
        FederatedServiceResolver resolver = new FederatedServiceResolverImpl();

        PreferentialEvaluationStrategy strategy = new PreferentialEvaluationStrategy(source, resolver);
        PreferenceRelationEvaluator preferenceRelationEvaluator = new PreferenceRelationEvaluator(source, resolver);
        BNLwinnowEvaluator evaluator = new BNLwinnowEvaluator();
        evaluator.setWindowSize(100);

        evaluator.setPreferenceRelationEvaluator(preferenceRelationEvaluator);
        evaluator.setEvaluationStrategy(strategy);
        strategy.setWinnowEvaluator(evaluator);

        CloseableIteration<BindingSet,QueryEvaluationException> iteration = strategy.evaluate(expr, new EmptyBindingSet());

        int count=0;
        while (iteration.hasNext()) {
            System.out.println("BNL: " + iteration.next());
            count++;
        }
        System.out.println("");

        assertEquals(3, count);
    }

    @Test
    public void testWinnowRewrite() throws Exception {

        TupleExpr expr = parser.parseQuery(query1,null).getTupleExpr();
        TupleExpr r = new WinnowRewriter().rewrite((Winnow) expr);
        String rewrited = new SPARQLQueryRenderer().render(new ParsedTupleQuery(r));

        Repository rep = new SailRepository(new MemoryStore());
        rep.initialize();

        RepositoryConnection conn = rep.getConnection();

        ValueFactory vf = rep.getValueFactory();


        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/price"), vf.createLiteral(14.75));

        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/price"), vf.createLiteral(13.50));

        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("qualityBooks"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/price"), vf.createLiteral(18.80));

        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0062059041"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/price"), vf.createLiteral(7.30));

        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0374164770"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/price"), vf.createLiteral(21.88));


        TupleQuery query = conn.prepareTupleQuery(QueryLanguage.SPARQL, rewrited);
        TupleQueryResult result = query.evaluate();

        System.out.println(rewrited);

        int count=0;
        while (result.hasNext()) {
            System.out.println("RW: " + result.next());
            count++;
        }
        System.out.println("");

        assertEquals(3, count);
    }


    @Test
    public void testPrefSails() throws Exception {

        SailRepositoryConfig config = new SailRepositoryConfig();
        MemoryStoreConfig memoryStoreConfig = new MemoryStoreConfig();
        PrefSailImplConfig prefSailConfig = new PrefSailImplConfig(memoryStoreConfig);
        config.setSailImplConfig(prefSailConfig);

        Optional<RepositoryFactory> factory = RepositoryRegistry.getInstance().get(config.getType());

        if (!factory.isPresent()) {
            throw new IOException("Factory not present!");
        }

        Repository rep = factory.get().getRepository(config);

        rep.initialize();

        RepositoryConnection conn = rep.getConnection();

        ValueFactory vf = rep.getValueFactory();

        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book1"), vf.createIRI("http://example.org/price"), vf.createLiteral(14.75));

        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book2"), vf.createIRI("http://example.org/price"), vf.createLiteral(13.50));

        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0679726691"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("qualityBooks"));
        conn.add(vf.createIRI("http://example.org/book3"), vf.createIRI("http://example.org/price"), vf.createLiteral(18.80));

        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0062059041"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("booksForLess"));
        conn.add(vf.createIRI("http://example.org/book4"), vf.createIRI("http://example.org/price"), vf.createLiteral(7.30));

        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/isbn"), vf.createLiteral("0374164770"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/vendor"), vf.createLiteral("lowestPrices"));
        conn.add(vf.createIRI("http://example.org/book5"), vf.createIRI("http://example.org/price"), vf.createLiteral(21.88));


        TupleQuery query = conn.prepareTupleQuery(Language.PrefSPARQL, query1);
        TupleQueryResult result = query.evaluate();

        int count=0;
        while (result.hasNext()) {
            System.out.println("PS: " + result.next());
            count++;
        }
        System.out.println("");

        assertEquals(3, count);
    }
}
