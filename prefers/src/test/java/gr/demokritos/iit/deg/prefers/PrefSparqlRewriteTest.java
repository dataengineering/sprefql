package gr.demokritos.iit.deg.prefers;

import gr.demokritos.iit.deg.prefers.experiments.Queries;
import gr.demokritos.iit.deg.prefers.experiments.Rewrites;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import gr.demokritos.iit.deg.prefers.rewrite.PrefSparqlRewriter;
import junit.framework.TestCase;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.junit.Test;

/**
 * Created by antonis on 3/4/2017.
 */
public class PrefSparqlRewriteTest extends TestCase {

    private PrefSPARQLParser parser = new PrefSPARQLParser();
    private PrefSparqlRewriter rewriter = new PrefSparqlRewriter();

    @Test
    public void testQueryLinkedMDB()
            throws Exception
    {
        String queries[] = {
                Queries.Q1,
                Queries.Q2,
                Queries.Q3,
                Queries.Q4,
                Queries.Q5,
                Queries.Q6,
                Queries.Q7,
        };

        String rewrited[] = {
                Rewrites.Q1,
                Rewrites.Q2,
                Rewrites.Q3,
                Rewrites.Q4,
                Rewrites.Q5,
                Rewrites.Q6,
                Rewrites.Q7,
        };

        String suffixes[] = {
                "afb1a82",
                "040c1db",
                "0373cbe",
                "1",
                "040c1db",
                "0373cbe",
                "1",
        };

        for (int i=0; i<queries.length; i++) {

            ParsedQuery q = parser.parseQuery(queries[i], null);
            assertNotNull(q);

            rewriter.setSuffix(suffixes[i]);
            String rewrite = rewriter.rewrite(queries[i]);
            String orig = rewriter.rewrite(rewrited[i]);

            assertEquals(orig, rewrite);
        }


    }

    @Test
    public void testQuery0()
            throws Exception
    {
        String simpleSparqlQuery = "PREFIX ex: <http://example.org/>\n" +
                "SELECT ?isbn ?vendor ?price\n" +
                "WHERE {\n" +
                "  ?s ex:isbn ?isbn .\n" +
                "  ?s ex:vendor ?vendor .\n" +
                "  ?s ex:price ?price .\n" +
                "}\n" +
                "PREFER (?i1 ?v1 ?p1) TO (?i2 ?v2 ?p2)\n" +
                "IF (?i1 = ?i2 && ?p1 < ?p2)";

        String result = "select ?isbn ?vendor ?price\n" +
                "where {\n" +
                "  ?s <http://example.org/isbn> ?isbn.\n" +
                "  ?s <http://example.org/vendor> ?vendor.\n" +
                "  ?s <http://example.org/price> ?price.\n" +
                "filter not exists {\n" +
                "{\n" +
                "  ?s_tmp <http://example.org/isbn> ?i1.\n" +
                "  ?s_tmp <http://example.org/vendor> ?v1.\n" +
                "  ?s_tmp <http://example.org/price> ?p1.\n" +
                "}\n" +
                "filter  ((?i1 = ?isbn) && (?p1 < ?price))\n" +
                "}\n" +
                "}\n";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(simpleSparqlQuery);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery1()
            throws Exception
    {
        String simpleSparqlQuery = "" +
                "SELECT ?o WHERE { ?s ?p ?o } \n" +
                "PREFER ?o1 TO ?o2 IF (?o1>?o2)";

        String result = "select ?o\nwhere {\n{\n" +
                "  ?s ?p ?o.\n" +
                "filter not exists {\n" +
                "  ?s_tmp ?p_tmp ?o1.\n" +
                "filter (?o1 > ?o)\n" +
                "}}\n}\n";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(simpleSparqlQuery);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }
    
    @Test
    public void testQuery2()
            throws Exception
    {
        String simpleSparqlQuery = "" +
                "SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
                "PREFER (?o5 ?o6) TO (?o3 ?o4) \n" +
                "IF (?o5>?o3) PRIOR TO (?o6<?o4)";

        String result = "select ?o1 ?o2\nwhere {\n" +
                "  ?s ?p1 ?o1.\n" +
                "  ?s ?p2 ?o2.\n" +
                "filter not exists {\n" +
                "  ?s_tmp ?p1_tmp ?o5.\n" +
                "  ?s_tmp ?p2_tmp ?o6.\n" +
                "filter  ((?o5 > ?o1) ||  ( (( !((?o5 > ?o1))) && ( !((?o1 > ?o5)))) && (?o6 < ?o2)))\n" +
                "}\n}\n";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(simpleSparqlQuery);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery3()
            throws Exception
    {
        String simpleSparqlQuery = "" +
                "SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
                "PREFER (?o5 ?o6) TO (?o3 ?o4) \n" +
                "IF (?o5>?o3) AND (?o6<?o4)";

        String result = "select ?o1 ?o2\nwhere {\n" +
                "  ?s ?p1 ?o1.\n" +
                "  ?s ?p2 ?o2.\n" +
                "filter not exists {\n" +
                "  ?s_tmp ?p1_tmp ?o5.\n" +
                "  ?s_tmp ?p2_tmp ?o6.\n" +
                "filter  ( ((?o5 > ?o1) && ( !((?o2 < ?o6)))) ||  ((?o6 < ?o2) && ( !((?o1 > ?o5)))))\n" +
                "}\n}\n";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(simpleSparqlQuery);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery4()
            throws Exception
    {
        String simpleSparqlQuery = "" +
                "SELECT ?x ?y WHERE { ?s ?p1 ?x . ?s ?p2 ?y } \n" +
                "PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
                "IF (?o1>?o3) PRIOR TO (?o2<?o4) PRIOR TO (?o1+?o2<?o3+?o4)";

        String result = "select ?x ?y\nwhere {\n" +
                "  ?s ?p1 ?x.\n" +
                "  ?s ?p2 ?y.\n" +
                "filter not exists {\n" +
                "  ?s_tmp ?p1_tmp ?o1.\n" +
                "  ?s_tmp ?p2_tmp ?o2.\n" +
                "filter  ((?o1 > ?x) ||  ( (( !((?o1 > ?x))) && ( !((?x > ?o1)))) " +
                    "&&  ((?o2 < ?y) ||  ( (( !((?o2 < ?y))) && ( !((?y < ?o2)))) " +
                    "&& ((?o1 + ?o2) < (?x + ?y))))))\n" +
                "}\n}\n";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(simpleSparqlQuery);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery5()
            throws Exception
    {
        String simpleSparqlQuery = "" +
                "SELECT ?x ?y WHERE { ?s ?p1 ?x . ?s ?p2 ?y } \n" +
                "PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
                "IF (?o1>?o3) AND (?o2<?o4) AND (?o1+?o2<?o3+?o4)";

        String result = "select ?x ?y\nwhere {\n" +
                "  ?s ?p1 ?x.\n" +
                "  ?s ?p2 ?y.\n" +
                "filter not exists {\n" +
                "  ?s_tmp ?p1_tmp ?o1.\n" +
                "  ?s_tmp ?p2_tmp ?o2.\n" +
                "filter  ( ((?o1 > ?x) && ( !( ( ((?y < ?o2) && ( !(((?o1 + ?o2) < (?x + ?y))))) ||  (((?x + ?y) < (?o1 + ?o2)) && ( !((?o2 < ?y)))))))) " +
                  "||  ( ( ((?o2 < ?y) && ( !(((?x + ?y) < (?o1 + ?o2))))) ||  (((?o1 + ?o2) < (?x + ?y)) && ( !((?y < ?o2))))) && ( !((?x > ?o1)))))\n" +
                "}\n}\n";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(simpleSparqlQuery);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery6()
            throws Exception
    {
        String simpleSparqlQuery = "" +
                "SELECT ?o5 ?o6 WHERE { ?s ?p1 ?o5 . ?s ?p2 ?o6 } \n" +
                "PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
                "IF (?o1=?o3) PRIOR TO (?o2<?o4) AND (?o2=?o4) PRIOR TO (?o1<?o3)";

        String result = "select ?o5 ?o6\nwhere {\n" +
                "  ?s ?p1 ?o5.\n" +
                "  ?s ?p2 ?o6.\n" +
                "filter not exists {\n" +
                "  ?s_tmp ?p1_tmp ?o1.\n" +
                "  ?s_tmp ?p2_tmp ?o2.\n" +
                "filter  ( ( ((?o1 = ?o5) ||  ( (( !((?o1 = ?o5))) && ( !((?o5 = ?o1)))) && (?o2 < ?o6))) " +
                    "&& ( !( ((?o6 = ?o2) ||  ( (( !((?o6 = ?o2))) && ( !((?o2 = ?o6)))) && (?o5 < ?o1)))))) " +
                      "||  ( ((?o2 = ?o6) ||  ( (( !((?o2 = ?o6))) && ( !((?o6 = ?o2)))) && (?o1 < ?o5))) " +
                    "&& ( !( ((?o5 = ?o1) ||  ( (( !((?o5 = ?o1))) && ( !((?o1 = ?o5)))) && (?o6 < ?o2)))))))\n" +
                "}\n}\n";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("tmp");
        String rewrite = rewriter.rewrite(simpleSparqlQuery);
        result = rewriter.rewrite(result);

        assertEquals(result, rewrite);
    }

    @Test
    public void testQuery7()
            throws Exception
    {
        String simpleSparqlQuery = "" +
                "SELECT ?o WHERE { ?s ?p ?o } \n" +
                "PREFER ?o TO ?o2 IF (?o>?o2)";

        ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);
        assertNotNull(q);
        try {
            String rewrite = rewriter.rewrite(simpleSparqlQuery);
            fail("expected exception was not occured.");
        } catch (RuntimeException e) {
            // do nothing
        }
    }

    @Test
    public void testQuery8()
            throws Exception
    {
        String sprefqlQuery = "" +
                "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
                "PREFIX dc: <http://purl.org/dc/terms/>\n" +
                "SELECT ?title ?genre \n" +
                " (xsd:float(?runtimeStr) \n" +
                "   as ?runtime) \n" +
                "WHERE {\n" +
                " ?s a movie:film.\n" +
                " ?s dc:title ?title.\n" +
                " ?s movie:genre ?g.\n" +
                " ?g movie:film_genre_name ?genre.\n" +
                " ?s movie:runtime ?runtimeStr.\n" +
                "}\n" +
                "PREFER (?title ?genre ?runtime) \n" +
                "TO (?otherTitle ?otherGenre ?otherRuntime) \n" +
                "IF (?genre = ?otherGenre && ?runtime > ?otherRuntime) ";

        String sparqlQuery = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n" +
                "select ?title ?genre (xsd:float(?runtimeStr) as ?runtime)\n" +
                "where {\n" +
                "  ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
                "  ?s <http://purl.org/dc/terms/title> ?title.\n" +
                "  ?s <http://data.linkedmdb.org/resource/movie/genre> ?g.\n" +
                "  ?g <http://data.linkedmdb.org/resource/movie/film_genre_name> ?genre.\n" +
                "  ?s <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
                "  filter not exists {\n" +
                "    ?s_afb1a82 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
                "    ?s_afb1a82 <http://purl.org/dc/terms/title> ?title_afb1a82.\n" +
                "    ?s_afb1a82 <http://data.linkedmdb.org/resource/movie/genre> ?g_afb1a82.\n" +
                "    ?g_afb1a82 <http://data.linkedmdb.org/resource/movie/film_genre_name> ?genre_afb1a82.\n" +
                "    ?s_afb1a82 <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_afb1a82.\n" +
                "    filter  ((?genre_afb1a82 = ?genre) && (xsd:float(?runtimeStr_afb1a82) > xsd:float(?runtimeStr)))\n" +
                "  }\n" +
                "}\n" ;

        ParsedQuery q = parser.parseQuery(sprefqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("afb1a82");
        String rewrite = rewriter.rewrite(sprefqlQuery);
        sparqlQuery = rewriter.rewrite(sparqlQuery);

        assertEquals(sparqlQuery, rewrite);
    }

    @Test
    public void testQuery9()
            throws Exception
    {
        String sprefqlQuery = "" +
                "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
                "PREFIX dc: <http://purl.org/dc/terms/>\n" +
                "SELECT ?title ?genre \n" +
                " (xsd:float(?runtimeStr) \n" +
                "   as ?runtime) \n" +
                "WHERE {\n" +
                " ?s a movie:film.\n" +
                " ?s dc:title ?title.\n" +
                " ?s movie:genre ?g.\n" +
                " ?g movie:film_genre_name ?genre.\n" +
                " ?s movie:runtime ?runtimeStr.\n" +
                "}\n" +
                "PREFER (?mytitle ?mygenre ?myruntime) \n" +
                "TO (?title ?genre ?runtime) \n" +
                "IF (?mygenre = ?genre && ?myruntime > ?runtime) ";

        String sparqlQuery = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \n" +
                "select ?title ?genre (xsd:float(?runtimeStr) as ?runtime)\n" +
                "where {\n" +
                "  ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
                "  ?s <http://purl.org/dc/terms/title> ?title.\n" +
                "  ?s <http://data.linkedmdb.org/resource/movie/genre> ?g.\n" +
                "  ?g <http://data.linkedmdb.org/resource/movie/film_genre_name> ?genre.\n" +
                "  ?s <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr.\n" +
                "  filter not exists {\n" +
                "    ?s_afb1a82 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://data.linkedmdb.org/resource/movie/film>.\n" +
                "    ?s_afb1a82 <http://purl.org/dc/terms/title> ?mytitle.\n" +
                "    ?s_afb1a82 <http://data.linkedmdb.org/resource/movie/genre> ?g_afb1a82.\n" +
                "    ?g_afb1a82 <http://data.linkedmdb.org/resource/movie/film_genre_name> ?mygenre.\n" +
                "    ?s_afb1a82 <http://data.linkedmdb.org/resource/movie/runtime> ?runtimeStr_afb1a82.\n" +
                "    filter  ((?mygenre = ?genre) && (xsd:float(?runtimeStr_afb1a82) > xsd:float(?runtimeStr)))\n" +
                "  }\n" +
                "}\n" ;

        ParsedQuery q = parser.parseQuery(sprefqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("afb1a82");
        String rewrite = rewriter.rewrite(sprefqlQuery);
        sparqlQuery = rewriter.rewrite(sparqlQuery);

        assertEquals(sparqlQuery, rewrite);
    }

    @Test
    public void testQuery10()
            throws Exception
    {
        String sprefqlQuery = "" +
                "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
                "PREFIX dc: <http://purl.org/dc/terms/>\n" +
                "SELECT ?title \n" +
                "  (xsd:float(?runtimeStr) as ?runtime)\n" +
                "  (xsd:int(substr(?dateStr,1,4)) as ?year) \n" +
                "WHERE {\n" +
                "  ?a a movie:film.\n" +
                "  ?a movie:genre ?g.\n" +
                "  ?g movie:film_genre_name \"Action\".\n" +
                "  ?a movie:runtime ?runtimeStr.\n" +
                "  ?a dc:title ?title.\n" +
                "  ?a dc:date ?dateStr.\n" +
                "}\n" +
                "PREFER (?title ?runtime ?year) \n" +
                "TO (?otherTitle ?otherRuntime ?otherYear) \n" +
                "IF (?runtime > ?otherRuntime) AND (?year > ?otherYear) ";

        String sparqlQuery = "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
                "PREFIX dc: <http://purl.org/dc/terms/>\n" +
                "SELECT ?title \n" +
                "  (xsd:float(?runtimeStr) as ?runtime)\n" +
                "  (xsd:int(substr(?dateStr,1,4)) as ?year) \n" +
                "WHERE {\n" +
                "  ?a a movie:film .\n" +
                "  ?a movie:genre ?g.\n" +
                "  ?g movie:film_genre_name \"Action\".\n" +
                "  ?a movie:runtime ?runtimeStr.\n" +
                "  ?a dc:title ?title.\n" +
                "  ?a dc:date ?dateStr.\n" +
                "  FILTER NOT EXISTS {\n" +
                "    ?a_040c1db a movie:film .\n" +
                "    ?a_040c1db movie:genre ?g_040c1db.\n" +
                "    ?g_040c1db movie:film_genre_name \"Action\".\n" +
                "    ?a_040c1db movie:runtime ?runtimeStr_040c1db.\n" +
                "    ?a_040c1db dc:title ?title_040c1db.\n" +
                "    ?a_040c1db dc:date ?dateStr_040c1db.\n" +
                "    FILTER (    ((xsd:float(?runtimeStr_040c1db) > xsd:float(?runtimeStr))  \n" +
                "          && ( !((xsd:int(substr(?dateStr, 1, 4)) > xsd:int(substr(?dateStr_040c1db, 1, 4)))))) \n" +
                "          ||    ((xsd:int(substr(?dateStr_040c1db, 1, 4)) > xsd:int(substr(?dateStr, 1, 4))) \n" +
                "          && ( !((xsd:float(?runtimeStr) > xsd:float(?runtimeStr_040c1db))))))\n" +
                "  }\n" +
                "}" ;

        ParsedQuery q = parser.parseQuery(sprefqlQuery, null);
        assertNotNull(q);

        rewriter.setSuffix("040c1db");
        String rewrite = rewriter.rewrite(sprefqlQuery);
        sparqlQuery = rewriter.rewrite(sparqlQuery);

        assertEquals(sparqlQuery, rewrite);
    }
}
