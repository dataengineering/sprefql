package gr.demokritos.iit.deg.prefers;

import gr.demokritos.iit.deg.prefers.algebra.RemoteQuery;
import gr.demokritos.iit.deg.prefers.algebra.SimplePref;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import gr.demokritos.iit.deg.prefers.commons.Language;
import gr.demokritos.iit.deg.prefers.evaluation.RemoteSiteEnforcer;
import gr.demokritos.iit.deg.prefers.evaluation.Site;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import gr.demokritos.iit.deg.prefers.sail.PrefSailImplConfig;
import junit.framework.TestCase;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.algebra.Exists;
import org.eclipse.rdf4j.query.algebra.Slice;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.config.RepositoryFactory;
import org.eclipse.rdf4j.repository.config.RepositoryRegistry;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by antonis on 20/4/2017.
 */
public class SiteEnforcerTest extends TestCase {

    @Test
    public void testWinnowSimple() {

        String query = "" +
                "PREFIX ex: <http://example.org/>" +
                "SELECT ?isbn ?vendor ?price\n" +
                "WHERE {\n" +
                "  ?s ex:isbn ?isbn .\n" +
                "  ?s ex:vendor ?vendor .\n" +
                "  ?s ex:price ?price .\n" +
                "}\n" +
                "PREFER (?i1 ?v1 ?p1) TO (?i2 ?v2 ?p2)\n" +
                "IF (?i1 = ?i2 && ?p1 < ?p2)";

        PrefSPARQLParser parser = new PrefSPARQLParser();
        TupleExpr expr = parser.parseQuery(query, null).getTupleExpr();

        RemoteSiteEnforcer enforcer = new RemoteSiteEnforcer();
        enforcer.setSite(new Site("http://dbpedia.org/sparql"));

        TupleExpr result = enforcer.enforceSite(expr);

        assertTrue(result instanceof Winnow);
        assertTrue(((Winnow) result).getArg() instanceof RemoteQuery);
        assertEquals(((Winnow) expr).getArg(), ((RemoteQuery) ((Winnow) result).getArg()).getArg());
        assertEquals(((Winnow) expr).getBase(), (((Winnow) result).getBase()));
        assertEquals(((Winnow) expr).getPrefered(), (((Winnow) result).getPrefered()));
        assertEquals(((Winnow) expr).getNonPrefered(), (((Winnow) result).getNonPrefered()));
        assertEquals(((Winnow) expr).getPreference(), (((Winnow) result).getPreference()));
    }


    @Test
    public void testWinnowExists() {

        String query = "" +
                "PREFIX dc: <http://purl.org/dc/terms/>\n" +
                "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
                "SELECT DISTINCT ?film ?title\n" +
                "WHERE {\n" +
                "  ?film a movie:film .\n" +
                "  ?film dc:title ?title.\n" +
                "}\n" +
                "PREFER (?f1 ?t1) TO (?f2 ?t2)\n" +
                "IF EXISTS { ?f1 movie:sequel ?f2 }";

        PrefSPARQLParser parser = new PrefSPARQLParser();
        TupleExpr expr = parser.parseQuery(query, null).getTupleExpr();

        RemoteSiteEnforcer enforcer = new RemoteSiteEnforcer();
        enforcer.setSite(new Site("http://linkedmdb.org/sparql"));

        TupleExpr result = enforcer.enforceSite(expr);


        assertTrue(result instanceof Winnow);
        assertTrue(((Winnow) result).getArg() instanceof RemoteQuery);
        assertTrue(((Exists) ((SimplePref) ((Winnow) result).getPreference()).getArg()).getSubQuery() instanceof RemoteQuery);
        assertEquals(((Winnow) expr).getArg(), ((RemoteQuery) ((Winnow) result).getArg()).getArg());
        assertEquals(((Winnow) expr).getBase(), (((Winnow) result).getBase()));
        assertEquals(((Winnow) expr).getPrefered(), (((Winnow) result).getPrefered()));
        assertEquals(((Winnow) expr).getNonPrefered(), (((Winnow) result).getNonPrefered()));
        assertEquals( ((Exists) ((SimplePref) ((Winnow) expr).getPreference()).getArg()).getSubQuery(),
                ((Slice) ((RemoteQuery) ((Exists) ((SimplePref) ((Winnow) result).getPreference()).getArg()).getSubQuery()).getArg()).getArg());
    }


    @Test
    public void testSparqlQuery() {

        String query = "" +
                "PREFIX ex: <http://example.org/>" +
                "SELECT ?isbn ?vendor ?price\n" +
                "WHERE {\n" +
                "  ?s ex:isbn ?isbn .\n" +
                "  ?s ex:vendor ?vendor .\n" +
                "  ?s ex:price ?price .\n" +
                "}";

        PrefSPARQLParser parser = new PrefSPARQLParser();
        TupleExpr expr = parser.parseQuery(query, null).getTupleExpr();

        RemoteSiteEnforcer enforcer = new RemoteSiteEnforcer();
        enforcer.setSite(new Site("http://dbpedia.org/sparql"));

        TupleExpr result = enforcer.enforceSite(expr);

        assertTrue(result instanceof RemoteQuery);
        assertEquals(expr, ((RemoteQuery) result).getArg());
    }

    @Test
    public void testOnlineSimple() throws IOException {

        String query = "" +
                "PREFIX dc: <http://purl.org/dc/terms/>\n" +
                "PREFIX movie: <http://data.linkedmdb.org/resource/movie/>\n" +
                "SELECT ?film (xsd:int(?runtimeStr) as ?runtime)\n" +
                "WHERE {\n" +
                "   ?f a movie:film .\n" +
                "   ?f dc:title ?film.\n" +
                "   ?f movie:genre ?g.\n" +
                "   ?g movie:film_genre_name \"Cult\".\n" +
                "   ?f movie:runtime ?runtimeStr \n" +
                "}\n" +
                "PREFER (?f1 ?r1) TO (?f2 ?r2) \n" +
                "IF (?r1 < ?r2)";

        String configFile = getPath("config.ttl");

        Model configModel = Rio.parse(new FileInputStream(new File(configFile)), "", RDFFormat.TURTLE);

        SailRepositoryConfig config = new SailRepositoryConfig();

        MemoryStoreConfig memoryStoreConfig = new MemoryStoreConfig();
        PrefSailImplConfig prefSailConfig = new PrefSailImplConfig(memoryStoreConfig);
        prefSailConfig.parse(configModel, null);
        config.setSailImplConfig(prefSailConfig);

        Optional<RepositoryFactory> factory = RepositoryRegistry.getInstance().get(config.getType());

        if (!factory.isPresent()) {
            throw new IOException("Factory not present!");
        }

        Repository repository = factory.get().getRepository(config);
        repository.initialize();

        RepositoryConnection conn = repository.getConnection();

        TupleQuery q = conn.prepareTupleQuery(Language.PrefSPARQL, query);
        TupleQueryResult result = q.evaluate();

        int count=0;
        while (result.hasNext()) {
            System.out.println("linkedmdb: " + result.next());
            count++;
        }
        System.out.println("");
        conn.close();

        assertEquals(1, count);
    }

    public String getPath(String fileName) {
        return getClass().getClassLoader().getResource(fileName).getPath();
    }
}
