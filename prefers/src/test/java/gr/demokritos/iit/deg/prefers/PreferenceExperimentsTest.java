package gr.demokritos.iit.deg.prefers;

import gr.demokritos.iit.deg.prefers.experiments.PrefExperiment;
import gr.demokritos.iit.deg.prefers.experiments.QueriesOld;
import gr.demokritos.iit.deg.prefers.experiments.SimpleExperiment;
import gr.demokritos.iit.deg.prefers.rewrite.PrefSparqlRewriter;
import junit.framework.TestCase;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by antonis on 12/4/2017.
 */
public class PreferenceExperimentsTest extends TestCase {

    private static String NLconfig = "config-nl.ttl";
    private static String BNLconfig = "config-bnl.ttl";

    private static String[] files = {
            "dumps/books.100.ttl"
    };

    @Test
    public void testBooksExperiment() throws Exception {

        for (String file: files) {
            int i = runPrefExperiment("NL", getPath(NLconfig), getPath(file), QueriesOld.booksPrefQuery);
            int j = runPrefExperiment("BNL", getPath(BNLconfig), getPath(file), QueriesOld.booksPrefQuery);
            int k = runSimpExperiment("RW", getPath(file), new PrefSparqlRewriter().rewrite(QueriesOld.booksPrefQuery));

            assertEquals(i,j);
            assertEquals(i,k);
        }
    }

    private int runPrefExperiment(String id, String config, String fileName, String query) throws IOException {
        PrefExperiment experiment = new PrefExperiment();
        experiment.setConfigFile(config);
        experiment.setInputFile(fileName);
        experiment.initialize();
        experiment.runQuery(query);
        return experiment.getLatestQueryResults();
    }

    private int runSimpExperiment(String id, String fileName, String query) throws IOException {
        SimpleExperiment experiment = new SimpleExperiment();
        experiment.setBaseSail(new MemoryStore());
        experiment.setInputFile(fileName);
        experiment.initialize();
        experiment.runQuery(query);
        return experiment.getLatestQueryResults();
    }

    private static String getPath(String fileName) {
        return PreferenceExperimentsTest.class.getClassLoader().getResource(fileName).getPath();
    }
}
