package gr.demokritos.iit.deg.prefers;

import gr.demokritos.iit.deg.prefers.evaluation.BNLchecker;
import gr.demokritos.iit.deg.prefers.experiments.Queries;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import junit.framework.TestCase;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.junit.Test;

/**
 * Created by antonis on 7/7/2017.
 */
public class TransitivityCheckTest extends TestCase {

    private PrefSPARQLParser parser = new PrefSPARQLParser();

    private static String all[] = {
            Queries.Q1,
            Queries.Q2,
            Queries.Q3,
            Queries.Q4,
            Queries.film_title_madmax
                    + "PREFER (?film1 ?title1)\n"
                    + "TO     (?film2 ?title2)\n"
                    + "IF (\n"
                    + "  ?title1 = \"Mad Max\" && ?title2 = \"Mad Max 4: Fury Road\" || \n"
                    + "  ?title1 = \"Mad Max 4: Fury Road\"  && ?title2 = \"Mad Max Beyond Thunderdome\" || \n"
                    + "  ?title1 = \"Mad Max Beyond Thunderdome\"  && ?title2 = \"Mad Max 2: The Road Warrior\"\n"
                    + ")\n",
            Queries.film_title_madmax
                    + "PREFER (?film1 ?title1)\n"
                    + "TO     (?film2 ?title2)\n"
                    + "IF (\n"
                    + "  ?title1 > ?title2\n"
                    + ")\n"
    };

    private static boolean results[] = {
            true,
            false,
            false,
            false,
            false,
            true
    };

    @Test
    public void testTransitivityAll() throws Exception {
        int i=0;
        for (String query: all) {
            TupleExpr tupleExpr = parser.parseQuery(query, null).getTupleExpr();
            BNLchecker checker = new BNLchecker(tupleExpr);
            assertEquals(results[i++], checker.hasTransitivePreference());
        }
    }
}
