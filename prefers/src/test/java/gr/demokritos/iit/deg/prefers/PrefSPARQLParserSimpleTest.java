/*******************************************************************************
 * Copyright (c) 2015 Eclipse RDF4J contributors, Aduna, and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Distribution License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *******************************************************************************/
package gr.demokritos.iit.deg.prefers;

import gr.demokritos.iit.deg.prefers.algebra.MultidimensionalPref;
import gr.demokritos.iit.deg.prefers.algebra.PrioritizedPref;
import gr.demokritos.iit.deg.prefers.algebra.SimplePref;
import gr.demokritos.iit.deg.prefers.algebra.Winnow;
import gr.demokritos.iit.deg.prefers.parser.PrefSPARQLParser;
import junit.framework.TestCase;
import org.eclipse.rdf4j.query.algebra.*;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.junit.Test;

import java.util.Arrays;


public class PrefSPARQLParserSimpleTest extends TestCase {

	private PrefSPARQLParser parser = new PrefSPARQLParser();

	private Var var_s = new Var("s");
	private Var var_p = new Var("p");
	private Var var_o = new Var("o");

	private Var var_p1 = new Var("p1");
	private Var var_p2 = new Var("p2");

	private Var var_o1 = new Var("o1");
	private Var var_o2 = new Var("o2");
	private Var var_o3 = new Var("o3");
	private Var var_o4 = new Var("o4");
	

	private Projection subquery1 = new Projection(
			new StatementPattern(var_s,var_p,var_o),
			new ProjectionElemList(new ProjectionElem("o"))
	);
	private Projection subquery2 = new Projection(
			new Join(
					new StatementPattern(var_s,var_p1,var_o1),
					new StatementPattern(var_s,var_p2,var_o2)
			),
			new ProjectionElemList(
					new ProjectionElem("o1"),
					new ProjectionElem("o2")
			)
	);

	private ValueExpr o1GTo2 = new Compare(var_o1, var_o2, Compare.CompareOp.GT);
	private ValueExpr o3EQo4 = new Compare(var_o3, var_o4, Compare.CompareOp.EQ);
	private ValueExpr o2EQo4 = new Compare(var_o2, var_o4, Compare.CompareOp.EQ);
	private ValueExpr o2LTo3 = new Compare(var_o2, var_o3, Compare.CompareOp.LT);

	@Test
	public void testQuery1()
		throws Exception
	{
		String simpleSparqlQuery = "" +
				"SELECT ?o WHERE { ?s ?p ?o } \n" +
				"PREFER ?o1 TO ?o2 IF (?o1>?o2)";

		TupleExpr result = new Winnow(subquery1,
						Arrays.asList(var_o1.getName()),
						Arrays.asList(var_o2.getName()),
						Arrays.asList(var_o.getName()),
						new SimplePref(o1GTo2));

		ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);

		assertNotNull(q);
		assertEquals(q.getTupleExpr(), result);
	}

	@Test
	public void testQuery2()
			throws Exception
	{
		String simpleSparqlQuery = "" +
				"SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
				"PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
				"IF (?o1>?o2) PRIOR TO (?o3=?o4)";

		TupleExpr result = new Winnow(subquery2,
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						Arrays.asList(var_o3.getName(),var_o4.getName()),
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						new PrioritizedPref(
								new SimplePref(o1GTo2),
								new SimplePref(o3EQo4)));

		ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);

		assertNotNull(q);
		assertEquals(q.getTupleExpr(), result);
	}

	@Test
	public void testQuery3()
			throws Exception
	{
		String simpleSparqlQuery = "" +
				"SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
				"PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
				"IF (?o1>?o2) AND (?o3=?o4) AND (?o2=?o4)";

		TupleExpr result = new Winnow(subquery2,
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						Arrays.asList(var_o3.getName(),var_o4.getName()),
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						new MultidimensionalPref(
								new SimplePref(o1GTo2),
								new MultidimensionalPref(
										new SimplePref(o3EQo4),
										new SimplePref(o2EQo4))));

		ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);

		assertNotNull(q);
		assertEquals(q.getTupleExpr(), result);
	}

	@Test
	public void testQuery4()
			throws Exception
	{
		String simpleSparqlQuery = "" +
				"SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
				"PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
				"IF (?o1>?o2) PRIOR TO (?o3=?o4) AND (?o3=?o4) PRIOR TO (?o2=?o4)";

		TupleExpr result = new Winnow(subquery2,
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						Arrays.asList(var_o3.getName(),var_o4.getName()),
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						new MultidimensionalPref(
								new PrioritizedPref(
										new SimplePref(o1GTo2),
										new SimplePref(o3EQo4)),
								new PrioritizedPref(
										new SimplePref(o3EQo4),
										new SimplePref(o2EQo4))));

		ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);

		assertNotNull(q);
		assertEquals(q.getTupleExpr(), result);
	}

	@Test
	public void testQuery5()
			throws Exception
	{
		String simpleSparqlQuery = "" +
				"SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
				"PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
				"IF ((?o1>?o2) AND (?o3=?o4))";

		TupleExpr result = new Winnow(subquery2,
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						Arrays.asList(var_o3.getName(),var_o4.getName()),
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						new MultidimensionalPref(
								new SimplePref(o1GTo2),
								new SimplePref(o3EQo4)));

		ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);

		assertNotNull(q);
		assertEquals(q.getTupleExpr(), result);
	}

	@Test
	public void testQuery6()
			throws Exception
	{
		String simpleSparqlQuery = "" +
				"SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
				"PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
				"IF (?o1>?o2) PRIOR TO ((?o3=?o4) AND (?o3=?o4)) PRIOR TO (?o2=?o4)";

		TupleExpr result = new Winnow(subquery2,
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						Arrays.asList(var_o3.getName(),var_o4.getName()),
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						new PrioritizedPref(
								new SimplePref(o1GTo2),
								new PrioritizedPref(
									new MultidimensionalPref(
											new SimplePref(o3EQo4),
											new SimplePref(o3EQo4)),
									new SimplePref(o2EQo4))));

		ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);

		assertNotNull(q);
		assertEquals(q.getTupleExpr(), result);
	}

	@Test
	public void testQuery7()
			throws Exception
	{
		String simpleSparqlQuery = "" +
				"SELECT ?o1 ?o2 WHERE { ?s ?p1 ?o1 . ?s ?p2 ?o2 } \n" +
				"PREFER (?o1 ?o2) TO (?o3 ?o4) \n" +
				"IF ((?o1>?o2) AND (?o3=?o4)) PRIOR TO (?o2<?o3)";

		TupleExpr result = new Winnow(subquery2,
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						Arrays.asList(var_o3.getName(),var_o4.getName()),
						Arrays.asList(var_o1.getName(),var_o2.getName()),
						new PrioritizedPref(
								new MultidimensionalPref(
										new SimplePref(o1GTo2),
										new SimplePref(o3EQo4)),
								new SimplePref(o2LTo3)));

		ParsedQuery q = parser.parseQuery(simpleSparqlQuery, null);

		assertNotNull(q);
		assertEquals(q.getTupleExpr(), result);
	}
}
