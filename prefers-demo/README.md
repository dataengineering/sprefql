# A simple SPREFQL Demo

This demo consists of 7 SPREFQL queries, over the [LinkedMDB](http://www.linkedmdb.org/) database using [PreferS](http://bitbucket.org/dataengineering/sprefql), our prototype implementation of SPREFQL.

## Requierements
* Docker version 17.05.0-ce
* docker-compose ver. 1.15.0

## How to deploy
Firstly, start the Virtuoso Store that contains the LinkedMDB database:
```
$ docker-compose up -d lmdb
```
Then, start the sprefql container:
```
$ docker-compose up -d prefers
```
You can now issue queries on http://localhost:8888/prefers-1.1/repositories/sparql
```
$ curl -X POST -H "Accept: text/csv" --data-urlencode "query=
PREFIX dc: <http://purl.org/dc/terms/>
PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
SELECT ?title ?genre (xsd:float(?runtimeStr) as ?runtime)
WHERE {
 ?s a movie:film.
 ?s dc:title ?title.
 ?s movie:genre ?g.
 ?g movie:film_genre_name ?genre.
 ?s movie:runtime ?runtimeStr.
}
PREFER (?title1 ?genre1 ?runtime1)
TO     (?title2 ?genre2 ?runtime2)
IF (?genre1 = ?genre2 &&  ?runtime1 > ?runtime2)
" http://localhost:8888/prefers-1.1/repositories/sparql
```
Alternatively, you it is possible connect with your browser in the following link http://localhost:8888/sprefql
To be able to do this, issue the following:
```
$ docker-compose exec prefers bash -c 'mkdir /usr/local/tomcat/webapps/sprefql/'
$ docker-compose exec prefers bash -c 'cp /opt/prefers/index.html /usr/local/tomcat/webapps/sprefql/'
```
If you want to use your own data, modify accordingly the definition of the virtuoso container in the docker-compose.yml file.
Also, the configuration file for the SPREFQL implementation can be found in prefers-conf/repository.ttl.
